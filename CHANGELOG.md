# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

### Changed

## [v1.7.0] - 2024-07-03

### Added

- Publish rustdoc documentation on GitLab Pages.

### Changed

- Breaking change: definition of the `DataAsset` structure is now aligned with ZDP.
- Update dependencies to latest versions.

### Removed

- Remove unused dependencies.

## [v1.6.1] - 2024-05-09

### Added

- Public function to verify multisig in `zscript::interface_ops::verify_multisig`.

## [v1.6.0] - 2024-03-30

### Added

- Added function to get a public key from a private key in `zn-zcrypto`.

### Changed

- Changed signature of a closure to check if a TX is in UTXO in `transaction_utils::tx_is_valid`.

## [v1.5.1] - 2023-12-28

### Added

- Automatic publishing in CI for GitLab.
- Test coverage reports in CI for GitLab.

### Changed

- Renamed crates to `zn-naom`, `zn-zcrypto` and `zn-script` to avoid name conflicts.

## [v1.5.0] - 2023-12-22

### Added

- Introduce a lexer for ZScript.
- Support step-by-step execution in ZScript.

### Changed

- Split modules into individual crates under the NAOM workspace.
- Code quality improvements.
- Refactor code in ZScript.

## [v1.4.0] - 2023-07-18

### Changed

- Sensible move of locktime check at chain level.
- Improvements to script constructions.
- Improvements to unit tests.
- Security update for dependencies.
- Improvements to code docs/comments.
- Preparation for op code integration in ZNP database.

### Removed

- Remove PubKeyHash in StackEntry enum.

## [v1.3.0] - 2023-05-23

### Added

- Implement conditional opcodes for scripts.
- Improve tests for scripts.
- Add burn transaction constructor.
- Add test for burn transaction.
- Add locktime check in tx_is_valid.

### Changed

- Substitute OP_RETURN opcode with OP_BURN.
- Bump network version to `5`.

## [v1.2.0] - 2023-03-31

### Added

- Add coinbase maturity timelock.
- Implement script opcodes.
- Add P2SH script type.

### Changed

- Improve `verify_multisig`.
- Improve `interpret_script`.
- Improve `is_valid_script`.
- Improve tests.

### Removed

- Remove coinbase related method and constant (moved to ZNP).
- Remove wildcard dependencies versioning.

## [v1.1.0] - 2022-10-24

### Added

- Create Receipt with metadata.
- Add support for receipt-based payments and receipt asset creation.
- Add extra utilities for receipt-asset creation and handling support.

### Changed

- Fix DDE transactions for added metadata field.
- Use the serialized transactions for header hash.
- Clean up BlockHeader and crypto.
- Re-add Nonce and add mining tx_hash to block header.
- Modify TxIn signable hash to use custom string format.
- Modify address construction.

## [v1.0.0] - 2021-12-06

### Added

- Add support for temporary address structure present on wallet.
- Add secretbox implementation using ring.
- Add new helpers for TX construction.
- Add pbkdf2 for key derivation.

### Changed

- Modify address construction.
- Fix serialization to match existing PublicKey/Signatures.

### Removed

- Remove sodiumoxide and use ring.

[1.6.0]: https://gitlab.com/zenotta/v2/NAOM/-/commits/v1.6.0?ref_type=tags
[1.5.1]: https://gitlab.com/zenotta/v2/NAOM/-/merge_requests/22
[1.5.0]: https://gitlab.com/zenotta/v2/NAOM/-/merge_requests/18
[1.4.0]: https://gitlab.com/zenotta/v2/NAOM/-/merge_requests/10
[1.3.0]: https://gitlab.com/zenotta/v2/NAOM/-/merge_requests/7
[1.2.0]: https://gitlab.com/zenotta/v2/NAOM/-/merge_requests/1
[1.1.0]: https://gitlab.com/zenotta/v2/NAOM/-/commits/v1.1.0?ref_type=tags
[1.0.0]: https://gitlab.com/zenotta/v2/NAOM/-/commits/v1.0.0?ref_type=tags
