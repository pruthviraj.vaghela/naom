import json
import sys
import datetime

def generate_html_report(json_file, html_output, project_name, execution_time):
    try:
        with open(json_file, 'r') as f:
            data = json.load(f)
    except FileNotFoundError:
        print(f"Error: JSON file '{json_file}' not found.")
        sys.exit(1)
    except json.JSONDecodeError as e:
        print(f"Error decoding JSON file '{json_file}': {e}")
        sys.exit(1)
    
    current_datetime = datetime.datetime.now().strftime('%d/%m/%Y %H:%M')  # Format adjusted to dd/mm/yyyy hh:mm

    html_content = f"<html><head><title>Semgrep Report for {project_name}</title></head><body>"
    html_content += f"<h1>Security Testing Report for {project_name}</h1>"
    html_content += f"<p><strong>Report Generated Date and Time (UTC):</strong> {current_datetime}</p>"
    html_content += f"<p><strong>Total Execution Time:</strong> {execution_time}</p>"
    html_content += "<hr>"

    if 'errors' in data:
        html_content += "<h2>Errors</h2>"
        for error in data['errors']:
            code = error.get('code', 'N/A')
            level = error.get('level', 'N/A')
            message = error.get('message', 'N/A')
            path = error.get('path', 'N/A')
            spans = error.get('spans', [])
            
            html_content += "<div style='border: 1px solid black; padding: 10px; margin-bottom: 10px;'>"
            html_content += f"<p><strong>Code:</strong> {code}</p>"
            html_content += f"<p><strong>Level:</strong> {level}</p>"
            html_content += f"<p><strong>Message:</strong> {message}</p>"
            html_content += f"<p><strong>Path:</strong> {path}</p>"
            
            if spans:
                html_content += "<p><strong>Spans:</strong></p>"
                html_content += "<ul>"
                for span in spans:
                    start = span.get('start', {})
                    end = span.get('end', {})
                    html_content += "<li>"
                    html_content += f"Start - Line: {start.get('line', 'N/A')}, Col: {start.get('col', 'N/A')}, Offset: {start.get('offset', 'N/A')}<br>"
                    html_content += f"End - Line: {end.get('line', 'N/A')}, Col: {end.get('col', 'N/A')}, Offset: {end.get('offset', 'N/A')}"
                    html_content += "</li>"
                html_content += "</ul>"
            
            html_content += "</div>"
    else:
        html_content += "<p>No errors found.</p>"

    if 'paths' in data:
        html_content += "<h2>Paths Scanned</h2>"
        html_content += "<ul>"
        for path in data['paths']['scanned']:
            html_content += f"<li>{path}</li>"
        html_content += "</ul>"

    if 'results' in data:
        html_content += "<h2>Results</h2>"
        for result in data['results']:
            check_id = result.get('check_id', 'N/A')
            path = result.get('path', 'N/A')
            start = result.get('start', {})
            end = result.get('end', {})
            extra = result.get('extra', {})
            message = extra.get('message', 'N/A')
            severity = extra.get('severity', 'N/A')
            fingerprint = extra.get('fingerprint', 'N/A')

            html_content += "<div style='border: 1px solid black; padding: 10px; margin-bottom: 10px;'>"
            html_content += f"<p><strong>Check ID:</strong> {check_id}</p>"
            html_content += f"<p><strong>Path:</strong> {path}</p>"
            html_content += f"<p><strong>Message:</strong> {message}</p>"
            html_content += f"<p><strong>Severity:</strong> {severity}</p>"
            html_content += f"<p><strong>Fingerprint:</strong> {fingerprint}</p>"
            
            html_content += "<p><strong>Location:</strong></p>"
            html_content += f"Start - Line: {start.get('line', 'N/A')}, Col: {start.get('col', 'N/A')}, Offset: {start.get('offset', 'N/A')}<br>"
            html_content += f"End - Line: {end.get('line', 'N/A')}, Col: {end.get('col', 'N/A')}, Offset: {end.get('offset', 'N/A')}"
            
            if 'metadata' in extra:
                metadata = extra['metadata']
                html_content += "<p><strong>Metadata:</strong></p>"
                html_content += "<ul>"
                for key, value in metadata.items():
                    if isinstance(value, list):
                        html_content += f"<li><strong>{key.capitalize()}:</strong> {', '.join(str(v) for v in value)}</li>"
                    else:
                        html_content += f"<li><strong>{key.capitalize()}:</strong> {value}</li>"
                html_content += "</ul>"

            html_content += "</div>"

    html_content += "</body></html>"
    
    with open(html_output, 'w') as f:
        f.write(html_content)

if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: python generate_html_report.py <input_json_file> <output_html_file> <project_name> <execution_time>")
        sys.exit(1)
    
    input_json_file = sys.argv[1]
    output_html_file = sys.argv[2]
    project_name = sys.argv[3]
    execution_time = sys.argv[4]
    
    generate_html_report(input_json_file, output_html_file, project_name, execution_time)
