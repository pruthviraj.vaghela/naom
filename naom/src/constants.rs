/*------- TRANSACTION CONSTANTS -------*/
pub const TX_PREPEND: u8 = b'g';
pub const RECEIPT_DEFAULT_DRS_TX_HASH: &str = "default_drs_tx_hash";
pub const MAX_METADATA_BYTES: usize = 800;
pub const TX_HASH_LENGTH: usize = 32;

/*------- ADDRESS CONSTANTS -------*/
pub const V0_ADDRESS_LENGTH: usize = 16;
pub const STANDARD_ADDRESS_LENGTH: usize = 64;
// Prepending character for a P2SH address
pub const P2SH_PREPEND: u8 = b'H';

/*------- NETWORK CONSTANTS --------*/
// Current network version: Always bump immediately after a version is deployed.
pub const NETWORK_VERSION: u32 = 6;
pub const NETWORK_VERSION_SERIALIZED: &[u8] = b"6";
// Network version 0
pub const NETWORK_VERSION_V0: u64 = 0;
// Network version to support temporary address structure on wallet
// TODO: Deprecate after addresses retire
pub const NETWORK_VERSION_TEMP: u64 = 99999;

/*------- VALUE HANDLING CONSTANTS --------*/
// Number of decimal places to divide to in display
pub const D_DISPLAY_PLACES_U64: u64 = 25200;
pub const D_DISPLAY_PLACES: f64 = 25200.0;
// Number of possible tokens in existence (10 billion)
pub const TOTAL_TOKENS: u64 = D_DISPLAY_PLACES_U64 * 10000000000;

/*------- ASSET CONSTANTS -------*/
// The value to sign/verify for receipt-based payments
pub const RECEIPT_ACCEPT_VAL: &str = "PAYMENT_ACCEPT";

/*------- BLOCK CONSTANTS --------*/
// Maximum number of bytes that a block can contain
pub const MAX_BLOCK_SIZE: usize = 1000;

/*------- SCRIPT CONSTANTS -------*/
// Maximum number of bytes pushable to the stack
pub const MAX_SCRIPT_ITEM_SIZE: u16 = 520;
// Maximum number of non-push operations per script
pub const MAX_OPS_PER_SCRIPT: u8 = 201;
// Maximum number of public keys per multisig
pub const MAX_PUB_KEYS_PER_MULTISIG: u8 = 20;
// Maximum script length in bytes
pub const MAX_SCRIPT_SIZE: u16 = 10000;
// Maximum number of values on script interpreter stack
pub const MAX_STACK_SIZE: u16 = 1000;
