/*------- ADDRESS CONSTANTS -------*/
pub const V0_ADDRESS_LENGTH: usize = 16;

/*------- NETWORK CONSTANTS --------*/
// Network version 0
pub const NETWORK_VERSION_V0: u64 = 0;
// Network version to support temporary address structure on wallet
// TODO: Deprecate after addresses retire
pub const NETWORK_VERSION_TEMP: u64 = 99999;
