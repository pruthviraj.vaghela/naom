use crate::constant::*;
use crate::{sha3_256, sign_ed25519::PublicKey};

/// Builds an address from a public key and a specified network version
///
/// ### Arguments
///
/// * `pub_key` - A public key to build an address from
/// * `address_version` - Network version to use for the address
pub fn construct_address_for(pub_key: &PublicKey, address_version: Option<u64>) -> String {
    match address_version {
        Some(NETWORK_VERSION_V0) => construct_address_v0(pub_key),
        Some(NETWORK_VERSION_TEMP) => construct_address_temp(pub_key),
        _ => construct_address(pub_key),
    }
}

/// Builds an address from a public key
///
/// ### Arguments
///
/// * `pub_key` - A public key to build an address from
pub fn construct_address(pub_key: &PublicKey) -> String {
    hex::encode(sha3_256::digest(pub_key.as_ref()))
}

/// Builds an old (network version 0) address from a public key
///
/// ### Arguments
///
/// * `pub_key` - A public key to build an address from
pub fn construct_address_v0(pub_key: &PublicKey) -> String {
    let first_pubkey_bytes = {
        // We used sodiumoxide serialization before with a 64 bit length prefix.
        // Make clear what we are using as this was not intended.
        let mut v = vec![32, 0, 0, 0, 0, 0, 0, 0];
        v.extend_from_slice(pub_key.as_ref());
        v
    };
    let mut first_hash = sha3_256::digest(&first_pubkey_bytes).to_vec();
    first_hash.truncate(V0_ADDRESS_LENGTH);
    hex::encode(first_hash)
}

/// Builds an address from a public key using the
/// temporary address scheme present on the wallet
///
/// TODO: Deprecate after addresses retire
///
/// ### Arguments
///
/// * `pub_key` - A public key to build an address from
pub fn construct_address_temp(pub_key: &PublicKey) -> String {
    let base64_encoding = base64::encode(pub_key.as_ref());
    let hex_decoded = decode_base64_as_hex(&base64_encoding);
    hex::encode(sha3_256::digest(&hex_decoded))
}

/// Decodes a base64 encoded string as hex, invalid character pairs are decoded up to the
/// first character. If the decoding up to the first character fails, a default value of 0
/// is used.
///
/// TODO: Deprecate after addresses retire
///
/// ### Arguments
///
/// * `s`   - Base64 encoded string
pub fn decode_base64_as_hex(s: &str) -> Vec<u8> {
    (0..s.len())
        .step_by(2)
        .map(|i| {
            u8::from_str_radix(&s[i..i + 2], 16)
                .or_else(|_| u8::from_str_radix(&s[i..i + 1], 16))
                .unwrap_or_default()
        })
        .collect()
}
