use crate::constants::*;
use crate::error_utils::*;
use crate::lang::{ConditionStack, Stack};
use crate::StackEntry;
use zcrypto::sha3_256;
use zcrypto::sign_ed25519 as sign;
use zcrypto::sign_ed25519::{PublicKey, Signature};
use zcrypto::utils::{construct_address, construct_address_temp, construct_address_v0};

/*---- FLOW CONTROL OPS ----*/

/// OP_NOP: Does nothing
///
/// Example: OP_NOP([x]) -> [x]
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_nop(_stack: &mut Stack) -> bool {
    let (op, desc) = (OPNOP, OPNOP_DESC);
    trace(op, desc);
    true
}

/// OP_IF: Checks if the top item on the stack is not 0 and executes the next block of instructions
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
/// * `cond_stack`  - mutable reference to the condition stack
pub fn op_if(stack: &mut Stack, cond_stack: &mut ConditionStack) -> bool {
    let (op, desc) = (OPIF, OPIF_DESC);
    trace(op, desc);
    let cond = if cond_stack.all_true() {
        let n = match stack.pop() {
            Some(StackEntry::Num(n)) => n,
            Some(_) => {
                error_item_type(op);
                return false;
            }
            _ => {
                error_num_items(op);
                return false;
            }
        };
        n != 0
    } else {
        false
    };
    cond_stack.push(cond);
    true
}

/// OP_NOTIF: Checks if the top item on the stack is 0 and executes the next block of instructions
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
/// * `cond_stack`  - mutable reference to the condition stack
pub fn op_notif(stack: &mut Stack, cond_stack: &mut ConditionStack) -> bool {
    let (op, desc) = (OPNOTIF, OPNOTIF_DESC);
    trace(op, desc);
    let cond = if cond_stack.all_true() {
        let n = match stack.pop() {
            Some(StackEntry::Num(n)) => n,
            Some(_) => {
                error_item_type(op);
                return false;
            }
            _ => {
                error_num_items(op);
                return false;
            }
        };
        n == 0
    } else {
        false
    };
    cond_stack.push(cond);
    true
}

/// OP_ELSE: Executes the next block of instructions if the previous OP_IF or OP_NOTIF was not executed
///
/// ### Arguments
///
/// * `cond_stack`  - mutable reference to the condition stack
pub fn op_else(cond_stack: &mut ConditionStack) -> bool {
    let (op, desc) = (OPELSE, OPELSE_DESC);
    trace(op, desc);
    if cond_stack.is_empty() {
        error_empty_condition(op);
        return false;
    }
    cond_stack.toggle();
    true
}

/// OP_ENDIF: Ends an OP_IF or OP_NOTIF block
///
/// ### Arguments
///
/// * `cond_stack`  - mutable reference to the condition stack
pub fn op_endif(cond_stack: &mut ConditionStack) -> bool {
    let (op, desc) = (OPENDIF, OPENDIF_DESC);
    trace(op, desc);
    if cond_stack.is_empty() {
        error_empty_condition(op);
        return false;
    }
    cond_stack.pop();
    true
}

/// OP_VERIFY: Removes the top item from the stack and ends execution with an error if it is 0
///
/// Example: OP_VERIFY([x]) -> []   if x != 0
///          OP_VERIFY([x]) -> fail if x == 0
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_verify(stack: &mut Stack) -> bool {
    let (op, desc) = (OPVERIFY, OPVERIFY_DESC);
    trace(op, desc);
    match stack.pop() {
        Some(x) => {
            if x == StackEntry::Num(0) {
                error_verify(op);
                return false;
            }
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    true
}

/// OP_BURN: Ends execution with an error
///
/// Example: OP_BURN([x]) -> fail
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_burn(_stack: &mut Stack) -> bool {
    let (op, desc) = (OPBURN, OPBURN_DESC);
    trace(op, desc);
    error_burn(op);
    false
}

/*---- STACK OPS ----*/

/// OP_TOALTSTACK: Moves the top item from the main stack to the top of the alt stack
///                
///
/// Example: OP_TOALTSTACK([x], []) -> [], [x]
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_toaltstack(stack: &mut Stack) -> bool {
    let (op, desc) = (OPTOALTSTACK, OPTOALTSTACK_DESC);
    trace(op, desc);
    match stack.pop() {
        Some(x) => stack.alt_stack.push(x),
        _ => {
            error_num_items(op);
            return false;
        }
    };
    true
}

/// OP_FROMALTSTACK: Moves the top item from the alt stack to the top of the main stack
///                  
/// Example: OP_FROMALTSTACK([], [x]) -> [x], []
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_fromaltstack(stack: &mut Stack) -> bool {
    let (op, desc) = (OPFROMALTSTACK, OPFROMALTSTACK_DESC);
    trace(op, desc);
    match stack.alt_stack.pop() {
        Some(x) => stack.push(x),
        _ => {
            error_num_items(op);
            return false;
        }
    };
    true
}

/// OP_2DROP: Removes the top two items from the stack
///
/// Example: OP_2DROP([x1, x2]) -> []
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_2drop(stack: &mut Stack) -> bool {
    let (op, desc) = (OP2DROP, OP2DROP_DESC);
    trace(op, desc);
    let len = stack.main_stack.len();
    if len < 2 {
        error_num_items(op);
        return false;
    }
    stack.main_stack.drain(len - 2..);
    true
}

/// OP_2DUP: Duplicates the top two items on the stack
///
/// Example: OP_2DUP([x1, x2]) -> [x1, x2, x1, x2]
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_2dup(stack: &mut Stack) -> bool {
    let (op, desc) = (OP2DUP, OP2DUP_DESC);
    trace(op, desc);
    let len = stack.main_stack.len();
    if len < 2 {
        error_num_items(op);
        return false;
    }
    let last_two = stack.main_stack[len - 2..].to_vec();
    stack.main_stack.extend_from_slice(&last_two);
    true
}

/// OP_3DUP: Duplicates the top three items on the stack
///
/// Example: OP_3DUP([x1, x2, x3]) -> [x1, x2, x3, x1, x2, x3]
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_3dup(stack: &mut Stack) -> bool {
    let (op, desc) = (OP3DUP, OP3DUP_DESC);
    trace(op, desc);
    let len = stack.main_stack.len();
    if len < 3 {
        error_num_items(op);
        return false;
    }
    let last_three = stack.main_stack[len - 3..].to_vec();
    stack.main_stack.extend_from_slice(&last_three);
    true
}

/// OP_2OVER: Copies the second-to-top pair of items to the top of the stack
///           
/// Example: OP_2OVER([x1, x2, x3, x4]) -> [x1, x2, x3, x4, x1, x2]
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_2over(stack: &mut Stack) -> bool {
    let (op, desc) = (OP2OVER, OP2OVER_DESC);
    trace(op, desc);
    let len = stack.main_stack.len();
    if len < 4 {
        error_num_items(op);
        return false;
    }
    let items = stack.main_stack[len - 4..len - 2].to_vec();
    stack.main_stack.extend_from_slice(&items);
    true
}

/// OP_2ROT: Moves the third-to-top pair of items to the top of the stack
///          
/// Example: OP_2ROT([x1, x2, x3, x4, x5, x6]) -> [x3, x4, x5, x6, x1, x2]
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_2rot(stack: &mut Stack) -> bool {
    let (op, desc) = (OP2ROT, OP2ROT_DESC);
    trace(op, desc);
    let len = stack.main_stack.len();
    if len < 6 {
        error_num_items(op);
        return false;
    }
    let items = stack.main_stack[len - 6..len - 4].to_vec();
    stack.main_stack.drain(len - 6..len - 4);
    stack.main_stack.extend_from_slice(&items);
    true
}

/// OP_2SWAP: Swaps the top two pairs of items on the stack
///
/// Example: OP_2SWAP([x1, x2, x3, x4]) -> [x3, x4, x1, x2]
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_2swap(stack: &mut Stack) -> bool {
    let (op, desc) = (OP2SWAP, OP2SWAP_DESC);
    trace(op, desc);
    let len = stack.main_stack.len();
    if len < 4 {
        error_num_items(op);
        return false;
    }
    stack.main_stack.swap(len - 4, len - 2);
    stack.main_stack.swap(len - 3, len - 1);
    true
}

/// OP_IFDUP: Duplicates the top item on the stack if it is not 0
///           
/// Example: OP_IFDUP([x]) -> [x, x] if x != 0
///          OP_IFDUP([x]) -> [x]    if x == 0
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_ifdup(stack: &mut Stack) -> bool {
    let (op, desc) = (OPIFDUP, OPIFDUP_DESC);
    trace(op, desc);
    match stack.last() {
        Some(x) => {
            if x != StackEntry::Num(0) {
                stack.push(x);
            }
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    true
}

/// OP_DEPTH: Pushes the stack size onto the stack
///
/// Example: OP_DEPTH([x1, x2, x3, x4]) -> [x1, x2, x3, x4, 4]
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_depth(stack: &mut Stack) -> bool {
    let (op, desc) = (OPDEPTH, OPDEPTH_DESC);
    trace(op, desc);
    stack.push(StackEntry::Num(stack.main_stack.len()))
}

/// OP_DROP: Removes the top item from the stack
///
/// Example: OP_DROP([x]) -> []
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_drop(stack: &mut Stack) -> bool {
    let (op, desc) = (OPDROP, OPDROP_DESC);
    trace(op, desc);
    match stack.pop() {
        Some(_x) => (),
        _ => {
            error_num_items(op);
            return false;
        }
    };
    true
}

/// OP_DUP: Duplicates the top item on the stack
///
/// Example: OP_DUP([x]) -> [x, x]
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_dup(stack: &mut Stack) -> bool {
    let (op, desc) = (OPDUP, OPDUP_DESC);
    trace(op, desc);
    match stack.last() {
        Some(x) => stack.push(x),
        _ => {
            error_num_items(op);
            return false;
        }
    };
    true
}

/// OP_NIP: Removes the second-to-top item from the stack
///
/// Example: OP_NIP([x1, x2]) -> [x2]
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_nip(stack: &mut Stack) -> bool {
    let (op, desc) = (OPNIP, OPNIP_DESC);
    trace(op, desc);
    let len = stack.main_stack.len();
    if len < 2 {
        error_num_items(op);
        return false;
    }
    stack.main_stack.remove(len - 2);
    true
}

/// OP_OVER: Copies the second-to-top item to the top of the stack
///
/// Example: OP_OVER([x1, x2]) -> [x1, x2, x1]
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_over(stack: &mut Stack) -> bool {
    let (op, desc) = (OPOVER, OPOVER_DESC);
    trace(op, desc);
    let len = stack.main_stack.len();
    if len < 2 {
        error_num_items(op);
        return false;
    }
    let x1 = stack.main_stack[len - 2].clone();
    stack.push(x1)
}

/// OP_PICK: Copies the (n+1)th-to-top item to the top of the stack, where n is the top item on the stack
///
/// Example: OP_PICK([x, x2, x1, x0, 3]) -> [x, x2, x1, x0, x]
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_pick(stack: &mut Stack) -> bool {
    let (op, desc) = (OPPICK, OPPICK_DESC);
    trace(op, desc);
    let n = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    let len = stack.main_stack.len();
    if n >= len {
        error_item_index(op);
        return false;
    }
    let x = stack.main_stack[len - 1 - n].clone();
    stack.push(x)
}

/// OP_ROLL: Moves the (n+1)th-to-top item to the top of the stack, where n is the top item on the stack
///
/// Example: OP_ROLL([x, x2, x1, x0, 3]) -> [x2, x1, x0, x]
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_roll(stack: &mut Stack) -> bool {
    let (op, desc) = (OPROLL, OPROLL_DESC);
    trace(op, desc);
    let n = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    let len = stack.main_stack.len();
    if n >= len {
        error_item_index(op);
        return false;
    }
    let x = stack.main_stack[len - 1 - n].clone();
    stack.main_stack.remove(len - 1 - n);
    stack.push(x)
}

/// OP_ROT: Moves the third-to-top item to the top of the stack
///
/// Example: OP_ROT([x1, x2, x3]) -> [x2, x3, x1]
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_rot(stack: &mut Stack) -> bool {
    let (op, desc) = (OPROT, OPROT_DESC);
    trace(op, desc);
    let len = stack.main_stack.len();
    if len < 3 {
        error_num_items(op);
        return false;
    }
    stack.main_stack.swap(len - 3, len - 2);
    stack.main_stack.swap(len - 2, len - 1);
    true
}

/// OP_SWAP: Swaps the top two items on the stack
///
/// Example: OP_SWAP([x1, x2]) -> [x2, x1]
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_swap(stack: &mut Stack) -> bool {
    let (op, desc) = (OPSWAP, OPSWAP_DESC);
    trace(op, desc);
    let len = stack.main_stack.len();
    if len < 2 {
        error_num_items(op);
        return false;
    }
    stack.main_stack.swap(len - 2, len - 1);
    true
}

/// OP_TUCK: Copies the top item behind the second-to-top item on the stack
///
/// Example: OP_TUCK([x1, x2]) -> [x2, x1, x2]
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_tuck(stack: &mut Stack) -> bool {
    let (op, desc) = (OPTUCK, OPTUCK_DESC);
    trace(op, desc);
    let len = stack.main_stack.len();
    if len < 2 {
        error_num_items(op);
        return false;
    }
    let x2 = stack.main_stack[len - 1].clone();
    stack.main_stack.insert(len - 2, x2);
    true
}

/*---- SPLICE OPS ----*/

/// OP_CAT: Concatenates the two strings on top of the stack
///
/// Example: OP_CAT([s1, s2]) -> [s1s2]
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_cat(stack: &mut Stack) -> bool {
    let (op, desc) = (OPCAT, OPCAT_DESC);
    trace(op, desc);
    let s2 = match stack.pop() {
        Some(StackEntry::Bytes(s)) => s,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    let s1 = match stack.pop() {
        Some(StackEntry::Bytes(s)) => s,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    if s1.len() + s2.len() > MAX_SCRIPT_ITEM_SIZE as usize {
        error_item_size(op);
        return false;
    }
    let cat = [s1, s2].join("");
    stack.push(StackEntry::Bytes(cat))
}

/// OP_SUBSTR: Extracts a substring from the third-to-top item on the stack
///
/// Example: OP_SUBSTR([s, n1, n2]) -> [s[n1..n1+n2-1]]
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_substr(stack: &mut Stack) -> bool {
    let (op, desc) = (OPSUBSTR, OPSUBSTR_DESC);
    trace(op, desc);
    let n2 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    let n1 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    let s = match stack.pop() {
        Some(StackEntry::Bytes(s)) => s,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    if n1 >= s.len() {
        error_item_index(op);
        return false;
    }
    if n2 > s.len() {
        error_item_index(op);
        return false;
    }
    if n1 + n2 > s.len() {
        error_item_index(op);
        return false;
    }
    let substr = s[n1..n1 + n2].to_string();
    stack.push(StackEntry::Bytes(substr))
}

/// OP_LEFT: Extracts a left substring from the second-to-top item on the stack
///
/// Example: OP_LEFT([s, n]) -> [s[..n-1]] if n < len(s)
///          OP_LEFT([s, n]) -> [s]        if n >= len(s)
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_left(stack: &mut Stack) -> bool {
    let (op, desc) = (OPLEFT, OPLEFT_DESC);
    trace(op, desc);
    let n = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    let s = match stack.pop() {
        Some(StackEntry::Bytes(s)) => s,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    if n >= s.len() {
        stack.push(StackEntry::Bytes(s))
    } else {
        let left = s[..n].to_string();
        stack.push(StackEntry::Bytes(left))
    }
}

/// OP_RIGHT: Extracts a right substring from the second-to-top item on the stack
///
/// Example: OP_RIGHT([s, n]) -> [s[n..]] if n < len(s)
///          OP_RIGHT([s, n]) -> [""]     if n >= len(s)
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_right(stack: &mut Stack) -> bool {
    let (op, desc) = (OPRIGHT, OPRIGHT_DESC);
    trace(op, desc);
    let n = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    let s = match stack.pop() {
        Some(StackEntry::Bytes(s)) => s,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    if n >= s.len() {
        stack.push(StackEntry::Bytes("".to_string()))
    } else {
        let right = s[n..].to_string();
        stack.push(StackEntry::Bytes(right))
    }
}

/// OP_SIZE: Computes the size in bytes of the string on top of the stack
///
/// Example: OP_SIZE([s]) -> [s, len(s)]
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_size(stack: &mut Stack) -> bool {
    let (op, desc) = (OPSIZE, OPSIZE_DESC);
    trace(op, desc);
    let s = match stack.last() {
        Some(StackEntry::Bytes(s)) => s,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    stack.push(StackEntry::Num(s.len()))
}

/*---- BITWISE LOGIC OPS ----*/

/// OP_INVERT: Computes bitwise NOT of the number on top of the stack
///
/// Example: OP_INVERT([n]) -> [!n]
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_invert(stack: &mut Stack) -> bool {
    let (op, desc) = (OPINVERT, OPINVERT_DESC);
    trace(op, desc);
    let n = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    stack.push(StackEntry::Num(!n))
}

/// OP_AND: Computes bitwise AND between the two numbers on top of the stack
///
/// Example: OP_AND([n1, n2]) -> [n1&n2]
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_and(stack: &mut Stack) -> bool {
    let (op, desc) = (OPAND, OPAND_DESC);
    trace(op, desc);
    let n2 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    let n1 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    stack.push(StackEntry::Num(n1 & n2))
}

/// OP_OR: Computes bitwise OR between the two numbers on top of the stack
///
/// Example: OP_OR([n1, n2]) -> [n1|n2]
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_or(stack: &mut Stack) -> bool {
    let (op, desc) = (OPOR, OPOR_DESC);
    trace(op, desc);
    let n2 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    let n1 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    stack.push(StackEntry::Num(n1 | n2))
}

/// OP_XOR: Computes bitwise XOR between the two numbers on top of the stack
///
/// Example: OP_XOR([n1, n2]) -> [n1^n2]
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_xor(stack: &mut Stack) -> bool {
    let (op, desc) = (OPXOR, OPXOR_DESC);
    trace(op, desc);
    let n2 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    let n1 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    stack.push(StackEntry::Num(n1 ^ n2))
}

/// OP_EQUAL: Substitutes the top two items on the stack with 1 if they are equal, with 0 otherwise.
///
/// Example: OP_EQUAL([x1, x2]) -> [1] if x1 == x2
///          OP_EQUAL([x1, x2]) -> [0] if x1 != x2
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_equal(stack: &mut Stack) -> bool {
    let (op, desc) = (OPEQUAL, OPEQUAL_DESC);
    trace(op, desc);
    let x2 = match stack.pop() {
        Some(x) => x,
        _ => {
            error_num_items(op);
            return false;
        }
    };
    let x1 = match stack.pop() {
        Some(x) => x,
        _ => {
            error_num_items(op);
            return false;
        }
    };
    if x1 == x2 {
        stack.push(StackEntry::Num(1))
    } else {
        stack.push(StackEntry::Num(0))
    }
}

/// OP_EQUALVERIFY: Computes OP_EQUAL and OP_VERIFY in sequence
///
/// Example: OP_EQUALVERIFY([x1, x2]) -> []   if x1 == x2
///          OP_EQUALVERIFY([x1, x2]) -> fail if x1 != x2
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_equalverify(stack: &mut Stack) -> bool {
    let (op, desc) = (OPEQUALVERIFY, OPEQUALVERIFY_DESC);
    trace(op, desc);
    let x2 = match stack.pop() {
        Some(x) => x,
        _ => {
            error_num_items(op);
            return false;
        }
    };
    let x1 = match stack.pop() {
        Some(x) => x,
        _ => {
            error_num_items(op);
            return false;
        }
    };
    if x1 != x2 {
        error_not_equal_items(op);
        return false;
    }
    true
}

/*---- ARITHMETIC OPS ----*/

/// OP_1ADD: Adds 1 to the number on top of the stack
///
/// Example: OP_1ADD([n]) -> [n+1]
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_1add(stack: &mut Stack) -> bool {
    let (op, desc) = (OP1ADD, OP1ADD_DESC);
    trace(op, desc);
    let n = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    match n.checked_add(1) {
        Some(n) => stack.push(StackEntry::Num(n)),
        _ => {
            error_overflow(op);
            false
        }
    }
}

/// OP_1SUB: Subtracts 1 from the number on top of the stack.
///
/// Example: OP_1SUB([n]) -> [n-1]
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_1sub(stack: &mut Stack) -> bool {
    let (op, desc) = (OP1SUB, OP1SUB_DESC);
    trace(op, desc);
    let n = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    match n.checked_sub(1) {
        Some(n) => stack.push(StackEntry::Num(n)),
        _ => {
            error_overflow(op);
            false
        }
    }
}

/// OP_2MUL: Multiplies by 2 the number on top of the stack
///
/// Example: OP_2MUL([n]) -> [n*2]
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_2mul(stack: &mut Stack) -> bool {
    let (op, desc) = (OP2MUL, OP2MUL_DESC);
    trace(op, desc);
    let n = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    match n.checked_mul(2) {
        Some(n) => stack.push(StackEntry::Num(n)),
        _ => {
            error_overflow(op);
            false
        }
    }
}

/// OP_2DIV: Divides by 2 the number on top of the stack
///
/// Example: OP_2DIV([n]) -> [n/2]
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_2div(stack: &mut Stack) -> bool {
    let (op, desc) = (OP2DIV, OP2DIV_DESC);
    trace(op, desc);
    let n = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    stack.push(StackEntry::Num(n / 2))
}

/// OP_NOT: Substitutes the number on top of the stack with 1 if it is equal to 0, with 0 otherwise
///
/// Example: OP_NOT([n]) -> [1] if n == 0
///          OP_NOT([n]) -> [0] if n != 0
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_not(stack: &mut Stack) -> bool {
    let (op, desc) = (OPNOT, OPNOT_DESC);
    trace(op, desc);
    let n = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    if n == 0 {
        stack.push(StackEntry::Num(1))
    } else {
        stack.push(StackEntry::Num(0))
    }
}

/// OP_0NOTEQUAL: Substitutes the number on top of the stack with 1 if it is not equal to 0, with 0 otherwise
///
/// Example: OP_0NOTEQUAL([n]) -> [1] if n != 0
///          OP_0NOTEQUAL([n]) -> [0] if n == 0
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_0notequal(stack: &mut Stack) -> bool {
    let (op, desc) = (OP0NOTEQUAL, OP0NOTEQUAL_DESC);
    trace(op, desc);
    let n = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    if n != 0 {
        stack.push(StackEntry::Num(1))
    } else {
        stack.push(StackEntry::Num(0))
    }
}

/// OP_ADD: Adds the two numbers on top of the stack
///
/// Example: OP_ADD([n1, n2]) -> [n1+n2]
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_add(stack: &mut Stack) -> bool {
    let (op, desc) = (OPADD, OPADD_DESC);
    trace(op, desc);
    let n2 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    let n1 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    match n1.checked_add(n2) {
        Some(n) => stack.push(StackEntry::Num(n)),
        _ => {
            error_overflow(op);
            false
        }
    }
}

/// OP_SUB: Subtracts the number on top of the stack from the second-to-top number on the stack
///
/// Example: OP_SUB([n1, n2]) -> [n1-n2]
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_sub(stack: &mut Stack) -> bool {
    let (op, desc) = (OPSUB, OPSUB_DESC);
    trace(op, desc);
    let n2 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    let n1 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    match n1.checked_sub(n2) {
        Some(n) => stack.push(StackEntry::Num(n)),
        _ => {
            error_overflow(op);
            false
        }
    }
}

/// OP_MUL: Multiplies the second-to-top number by the number on top of the stack
///
/// Example: OP_MUL([n1, n2]) -> [n1*n2]
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_mul(stack: &mut Stack) -> bool {
    let (op, desc) = (OPMUL, OPMUL_DESC);
    trace(op, desc);
    let n2 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    let n1 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    match n1.checked_mul(n2) {
        Some(n) => stack.push(StackEntry::Num(n)),
        _ => {
            error_overflow(op);
            false
        }
    }
}

/// OP_DIV: Divides the second-to-top number by the number on top of the stack
///
/// Example: OP_DIV([n1, n2]) -> [n1/n2]
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_div(stack: &mut Stack) -> bool {
    let (op, desc) = (OPDIV, OPDIV_DESC);
    trace(op, desc);
    let n2 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    let n1 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    match n1.checked_div(n2) {
        Some(n) => stack.push(StackEntry::Num(n)),
        _ => {
            error_div_zero(op);
            false
        }
    }
}

/// OP_MOD: Computes the remainder of the division of the second-to-top number by the number on top of the stack
///
/// Example: OP_MOD([n1, n2]) -> [n1%n2]
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_mod(stack: &mut Stack) -> bool {
    let (op, desc) = (OPMOD, OPMOD_DESC);
    trace(op, desc);
    let n2 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    let n1 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    match n1.checked_rem(n2) {
        Some(n) => stack.push(StackEntry::Num(n)),
        _ => {
            error_div_zero(op);
            false
        }
    }
}

/// OP_LSHIFT: Computes the left shift of the second-to-top number by the number on top of the stack
///
/// Example: OP_LSHIFT([n1, n2]) -> [n1<<n2]
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_lshift(stack: &mut Stack) -> bool {
    let (op, desc) = (OPLSHIFT, OPLSHIFT_DESC);
    trace(op, desc);
    let n2 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    let n1 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    match n1.checked_shl(n2 as u32) {
        Some(n) => stack.push(StackEntry::Num(n)),
        _ => {
            error_div_zero(op);
            false
        }
    }
}

/// OP_RSHIFT: Computes the right shift of the second-to-top number by the number on top of the stack
///
/// Example: OP_RSHIFT([n1, n2]) -> [n1>>n2]
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_rshift(stack: &mut Stack) -> bool {
    let (op, desc) = (OPRSHIFT, OPRSHIFT_DESC);
    trace(op, desc);
    let n2 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    let n1 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    match n1.checked_shr(n2 as u32) {
        Some(n) => stack.push(StackEntry::Num(n)),
        _ => {
            error_div_zero(op);
            false
        }
    }
}

/// OP_BOOLAND: Substitutes the two numbers on top of the stack with 1 if they are both non-zero, with 0 otherwise
///
/// Example: OP_BOOLAND([n1, n2]) -> [1] if n1 != 0 and n2 != 0
///          OP_BOOLAND([n1, n2]) -> [0] if n1 == 0 or n2 == 0
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_booland(stack: &mut Stack) -> bool {
    let (op, desc) = (OPBOOLAND, OPBOOLAND_DESC);
    trace(op, desc);
    let n2 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    let n1 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    if n1 != 0 && n2 != 0 {
        stack.push(StackEntry::Num(1))
    } else {
        stack.push(StackEntry::Num(0))
    }
}

/// OP_BOOLOR: Substitutes the two numbers on top of the stack with 1 if they are not both 0, with 0 otherwise
///
/// Example: OP_BOOLOR([n1, n2]) -> [1] if n1 != 0 or n2 != 0
///          OP_BOOLOR([n1, n2]) -> [0] if n1 == 0 and n2 == 0
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_boolor(stack: &mut Stack) -> bool {
    let (op, desc) = (OPBOOLOR, OPBOOLOR_DESC);
    trace(op, desc);
    let n2 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    let n1 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    if n1 != 0 || n2 != 0 {
        stack.push(StackEntry::Num(1))
    } else {
        stack.push(StackEntry::Num(0))
    }
}

/// OP_NUMEQUAL: Substitutes the two numbers on top of the stack with 1 if they are equal, with 0 otherwise
///
/// Example: OP_NUMEQUAL([n1, n2]) -> [1] if n1 == n2
///          OP_NUMEQUAL([n1, n2]) -> [0] if n1 != n2
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_numequal(stack: &mut Stack) -> bool {
    let (op, desc) = (OPNUMEQUAL, OPNUMEQUAL_DESC);
    trace(op, desc);
    let n2 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    let n1 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    if n1 == n2 {
        stack.push(StackEntry::Num(1))
    } else {
        stack.push(StackEntry::Num(0))
    }
}

/// OP_NUMEQUALVERIFY: Computes OP_NUMEQUAL and OP_VERIFY in sequence
///
/// Example: OP_NUMEQUALVERIFY([n1, n2]) -> []   if n1 == n2
///          OP_NUMEQUALVERIFY([n1, n2]) -> fail if n1 != n2
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_numequalverify(stack: &mut Stack) -> bool {
    let (op, desc) = (OPNUMEQUALVERIFY, OPNUMEQUALVERIFY_DESC);
    trace(op, desc);
    let n2 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    let n1 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    if n1 != n2 {
        error_not_equal_items(op);
        return false;
    }
    true
}

/// OP_NUMNOTEQUAL: Substitutes the two numbers on top of the stack with 1 if they are not equal, with 0 otherwise
///
/// Example: OP_NUMNOTEQUAL([n1, n2]) -> [1] if n1 != n2
///          OP_NUMNOTEQUAL([n1, n2]) -> [0] if n1 == n2
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_numnotequal(stack: &mut Stack) -> bool {
    let (op, desc) = (OPNUMNOTEQUAL, OPNUMNOTEQUAL_DESC);
    trace(op, desc);
    let n2 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    let n1 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    if n1 != n2 {
        stack.push(StackEntry::Num(1))
    } else {
        stack.push(StackEntry::Num(0))
    }
}

/// OP_LESSTHAN: Substitutes the two numbers on top of the stack with 1 if the second-to-top is less than the top item, with 0 otherwise
///
/// Example: OP_LESSTHAN([n1, n2]) -> [1] if n1 < n2
///          OP_LESSTHAN([n1, n2]) -> [0] if n1 >= n2
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_lessthan(stack: &mut Stack) -> bool {
    let (op, desc) = (OPLESSTHAN, OPLESSTHAN_DESC);
    trace(op, desc);
    let n2 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    let n1 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    if n1 < n2 {
        stack.push(StackEntry::Num(1))
    } else {
        stack.push(StackEntry::Num(0))
    }
}

/// OP_GREATERTHAN: Substitutes the two numbers on top of the stack with 1 if the second-to-top is greater than the top item, with 0 otherwise
///
/// Example: OP_GREATERTHAN([n1, n2]) -> [1] if n1 > n2
///          OP_GREATERTHAN([n1, n2]) -> [0] if n1 <= n2
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_greaterthan(stack: &mut Stack) -> bool {
    let (op, desc) = (OPGREATERTHAN, OPGREATERTHAN_DESC);
    trace(op, desc);
    let n2 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    let n1 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    if n1 > n2 {
        stack.push(StackEntry::Num(1))
    } else {
        stack.push(StackEntry::Num(0))
    }
}

/// OP_LESSTHANOREQUAL: Substitutes the two numbers on top of the stack with 1 if the second-to-top is less than or equal to the top item, with 0 otherwise
///
/// Example: OP_LESSTHANOREQUAL([n1, n2]) -> [1] if n1 <= n2
///          OP_LESSTHANOREQUAL([n1, n2]) -> [0] if n1 > n2
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_lessthanorequal(stack: &mut Stack) -> bool {
    let (op, desc) = (OPLESSTHANOREQUAL, OPLESSTHANOREQUAL_DESC);
    trace(op, desc);
    let n2 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    let n1 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    if n1 <= n2 {
        stack.push(StackEntry::Num(1))
    } else {
        stack.push(StackEntry::Num(0))
    }
}

/// OP_GREATERTHANOREQUAL: Substitutes the two numbers on top of the stack with 1 if the second-to-top is greater than or equal to the top item, with 0 otherwise
///
/// Example: OP_GREATERTHANOREQUAL([n1, n2]) -> [1] if n1 >= n2
///          OP_GREATERTHANOREQUAL([n1, n2]) -> [0] if n1 < n2
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_greaterthanorequal(stack: &mut Stack) -> bool {
    let (op, desc) = (OPGREATERTHANOREQUAL, OPGREATERTHANOREQUAL_DESC);
    trace(op, desc);
    let n2 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    let n1 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    if n1 >= n2 {
        stack.push(StackEntry::Num(1))
    } else {
        stack.push(StackEntry::Num(0))
    }
}

/// OP_MIN: Substitutes the two numbers on top of the stack with the minimum between the two
///
/// Example: OP_MIN([n1, n2]) -> [n1] if n1 <= n2
///          OP_MIN([n1, n2]) -> [n2] if n1 > n2
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_min(stack: &mut Stack) -> bool {
    let (op, desc) = (OPMIN, OPMIN_DESC);
    trace(op, desc);
    let n2 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    let n1 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    stack.push(StackEntry::Num(n1.min(n2)))
}

/// OP_MAX: Substitutes the two numbers on top of the stack with the maximum between the two
///
/// Example: OP_MAX([n1, n2]) -> [n1] if n1 >= n2
///          OP_MAX([n1, n2]) -> [n2] if n1 < n2
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_max(stack: &mut Stack) -> bool {
    let (op, desc) = (OPMAX, OPMAX_DESC);
    trace(op, desc);
    let n2 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    let n1 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    stack.push(StackEntry::Num(n1.max(n2)))
}

/// OP_WITHIN: Substitutes the three numbers on top of the the stack with 1 if the third-to-top is greater or equal to the second-to-top and less than the top item, with 0 otherwise
///
/// Example: OP_WITHIN([n1, n2, n3]) -> [1] if n1 >= n2 and n1 < n3
///          OP_WITHIN([n1, n2, n3]) -> [0] if n1 < n2 or n1 >= n3
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_within(stack: &mut Stack) -> bool {
    let (op, desc) = (OPWITHIN, OPWITHIN_DESC);
    trace(op, desc);
    let n3 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    let n2 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    let n1 = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    if n1 >= n2 && n1 < n3 {
        stack.push(StackEntry::Num(1))
    } else {
        stack.push(StackEntry::Num(0))
    }
}

/*---- CRYPTO OPS ----*/

/// OP_SHA3: Hashes the top item on the stack using SHA3-256
///
/// Example: OP_SHA3([x]) -> [SHA3-256(x)]
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_sha3(stack: &mut Stack) -> bool {
    let (op, desc) = (OPSHA3, OPSHA3_DESC);
    trace(op, desc);
    let data = match stack.pop() {
        Some(StackEntry::Signature(sig)) => sig.as_ref().to_owned(),
        Some(StackEntry::PubKey(pk)) => pk.as_ref().to_owned(),
        Some(StackEntry::Bytes(s)) => s.as_bytes().to_owned(),
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    let hash = hex::encode(sha3_256::digest(&data));
    stack.push(StackEntry::Bytes(hash))
}

/// OP_HASH256: Creates standard address from public key and pushes it onto the stack
///
/// Example: OP_HASH256([pk]) -> [addr]
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_hash256(stack: &mut Stack) -> bool {
    let (op, desc) = (OPHASH256, OPHASH256_DESC);
    trace(op, desc);
    let pk = match stack.pop() {
        Some(StackEntry::PubKey(pk)) => pk,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    let addr = construct_address(&pk);
    stack.push(StackEntry::Bytes(addr))
}

/// OP_HASH256_V0: Creates v0 address from public key and pushes it onto the stack
///
/// Example: OP_HASH256_V0([pk]) -> [addr_v0]
///
/// Info: Support for old 32-byte addresses
///
/// TODO: Deprecate after addresses retire
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_hash256_v0(stack: &mut Stack) -> bool {
    let (op, desc) = (OPHASH256V0, OPHASH256V0_DESC);
    trace(op, desc);
    let pk = match stack.pop() {
        Some(StackEntry::PubKey(pk)) => pk,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    let addr_v0 = construct_address_v0(&pk);
    stack.push(StackEntry::Bytes(addr_v0))
}

/// OP_HASH256_TEMP: Creates temporary address from public key and pushes it onto the stack
///
/// Example: OP_HASH256_TEMP([pk]) -> [addr_temp]
///
/// Info: Support for temporary address scheme used in wallet
///
/// TODO: Deprecate after addresses retire
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_hash256_temp(stack: &mut Stack) -> bool {
    let (op, desc) = (OPHASH256TEMP, OPHASH256TEMP_DESC);
    trace(op, desc);
    let pk = match stack.pop() {
        Some(StackEntry::PubKey(pk)) => pk,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    let addr_temp = construct_address_temp(&pk);
    stack.push(StackEntry::Bytes(addr_temp))
}

/// OP_CHECKSIG: Pushes 1 onto the stack if the signature is valid, 0 otherwise
///
/// Example: OP_CHECKSIG([msg, sig, pk]) -> [1] if Verify(sig, msg, pk) == 1
///          OP_CHECKSIG([msg, sig, pk]) -> [0] if Verify(sig, msg, pk) == 0
///
/// Info: It allows signature verification on arbitrary messsages, not only transactions
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_checksig(stack: &mut Stack) -> bool {
    let (op, desc) = (OPCHECKSIG, OPCHECKSIG_DESC);
    trace(op, desc);
    let pk = match stack.pop() {
        Some(StackEntry::PubKey(pk)) => pk,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    let sig = match stack.pop() {
        Some(StackEntry::Signature(sig)) => sig,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    let msg = match stack.pop() {
        Some(StackEntry::Bytes(s)) => s,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    if !sign::verify_detached(&sig, msg.as_bytes(), &pk) {
        stack.push(StackEntry::Num(0))
    } else {
        stack.push(StackEntry::Num(1))
    }
}

/// OP_CHECKSIGVERIFY: Runs OP_CHECKSIG and OP_VERIFY in sequence
///
/// Example: OP_CHECKSIGVERIFY([msg, sig, pk]) -> []   if Verify(sig, msg, pk) == 1
///          OP_CHECKSIGVERIFY([msg, sig, pk]) -> fail if Verify(sig, msg, pk) == 0
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_checksigverify(stack: &mut Stack) -> bool {
    let (op, desc) = (OPCHECKSIGVERIFY, OPCHECKSIGVERIFY_DESC);
    trace(op, desc);
    let pk = match stack.pop() {
        Some(StackEntry::PubKey(pk)) => pk,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    let sig = match stack.pop() {
        Some(StackEntry::Signature(sig)) => sig,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    let msg = match stack.pop() {
        Some(StackEntry::Bytes(s)) => s,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    if !sign::verify_detached(&sig, msg.as_bytes(), &pk) {
        error_invalid_signature(op);
        return false;
    }
    true
}

/// OP_CHECKMULTISIG: Pushes 1 onto the stack if the m-of-n multi-signature is valid, 0 otherwise
///
/// Example: OP_CHECKMULTISIG([msg, sig1, sig2, m, pk1, pk2, pk3, n]) -> [1] if Verify(sig1, sig2, msg, pk1, pk2, pk3) == 1
///          OP_CHECKMULTISIG([msg, sig1, sig2, m, pk1, pk2, pk3, n]) -> [0] if Verify(sig1, sig2, msg, pk1, pk2, pk3) == 0
///
/// Info: It allows multi-signature verification on arbitrary messsages, not only transactions
///       Ordering of signatures and public keys is not relevant
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_checkmultisig(stack: &mut Stack) -> bool {
    let (op, desc) = (OPCHECKMULTISIG, OPCHECKMULTISIG_DESC);
    trace(op, desc);
    let n = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    if n > MAX_PUB_KEYS_PER_MULTISIG as usize {
        error_num_pubkeys(op);
        return false;
    }
    let mut pks = Vec::new();
    while let Some(StackEntry::PubKey(_)) = stack.last() {
        if let Some(StackEntry::PubKey(pk)) = stack.pop() {
            pks.push(pk);
        }
    }
    if pks.len() != n {
        error_num_pubkeys(op);
        return false;
    }
    let m = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    if m > n {
        error_num_signatures(op);
        return false;
    }
    let mut sigs = Vec::new();
    while let Some(StackEntry::Signature(_)) = stack.last() {
        if let Some(StackEntry::Signature(sig)) = stack.pop() {
            sigs.push(sig);
        }
    }
    if sigs.len() != m {
        error_num_signatures(op);
        return false;
    }
    let msg = match stack.pop() {
        Some(StackEntry::Bytes(s)) => s,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    if !verify_multisig(&sigs, &msg, &mut pks) {
        stack.push(StackEntry::Num(0))
    } else {
        stack.push(StackEntry::Num(1))
    }
}

/// OP_CHECKMULTISIGVERIFY: Runs OP_CHECKMULTISIG and OP_VERIFY in sequence
///
/// Example: OP_CHECKMULTISIGVERIFY([msg, sig1, sig2, m, pk1, pk2, pk3, n]) -> []   if Verify(sig1, sig2, msg, pk1, pk2, pk3) == 1
///          OP_CHECKMULTISIGVERIFY([msg, sig1, sig2, m, pk1, pk2, pk3, n]) -> fail if Verify(sig1, sig2, msg, pk1, pk2, pk3) == 0
///
/// ### Arguments
///
/// * `stack`  - mutable reference to the stack
pub fn op_checkmultisigverify(stack: &mut Stack) -> bool {
    let (op, desc) = (OPCHECKMULTISIGVERIFY, OPCHECKMULTISIGVERIFY_DESC);
    trace(op, desc);
    let n = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    if n > MAX_PUB_KEYS_PER_MULTISIG as usize {
        error_num_pubkeys(op);
        return false;
    }
    let mut pks = Vec::new();
    while let Some(StackEntry::PubKey(_)) = stack.last() {
        if let Some(StackEntry::PubKey(pk)) = stack.pop() {
            pks.push(pk);
        }
    }
    if pks.len() != n {
        error_num_pubkeys(op);
        return false;
    }
    let m = match stack.pop() {
        Some(StackEntry::Num(n)) => n,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    if m > n {
        error_num_signatures(op);
        return false;
    }
    let mut sigs = Vec::new();
    while let Some(StackEntry::Signature(_)) = stack.last() {
        if let Some(StackEntry::Signature(sig)) = stack.pop() {
            sigs.push(sig);
        }
    }
    if sigs.len() != m {
        error_num_signatures(op);
        return false;
    }
    let msg = match stack.pop() {
        Some(StackEntry::Bytes(s)) => s,
        Some(_) => {
            error_item_type(op);
            return false;
        }
        _ => {
            error_num_items(op);
            return false;
        }
    };
    if !verify_multisig(&sigs, &msg, &mut pks) {
        error_invalid_multisignature(op);
        return false;
    }
    true
}

/// Verifies an m-of-n multi-signature
///
/// ### Arguments
///
/// * `sigs` - signatures to verify
/// * `msg`  - data to verify against
/// * `pks`  - public keys to match against
pub fn verify_multisig(sigs: &[Signature], msg: &String, pks: &mut Vec<PublicKey>) -> bool {
    let mut num_valid_sigs = 0;
    for sig in sigs {
        if let Some((index, _)) = pks
            .iter()
            .enumerate()
            .find(|(_, pk)| sign::verify_detached(sig, msg.as_bytes(), pk))
        {
            num_valid_sigs += 1;
            pks.remove(index);
        }
    }
    num_valid_sigs == sigs.len()
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::lang::Script;
    use crate::OpCodes;

    /*---- FLOW CONTROL OPS ----*/

    #[test]
    // Test OP_NOP
    fn test_nop() {
        // op_nop([1]) -> [1]
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        let v: Vec<StackEntry> = vec![StackEntry::Num(1)];
        op_nop(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_nop([]) -> []
        let mut stack = Stack::new();
        let v: Vec<StackEntry> = vec![];
        op_nop(&mut stack);
        assert_eq!(stack.main_stack, v)
    }

    #[test]
    // Test OP_IF
    fn test_if() {
        // op_if([1], {0,None}) -> [], {1,None}
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        let mut cond_stack = ConditionStack::new();
        let v: Vec<StackEntry> = vec![];
        op_if(&mut stack, &mut cond_stack);
        assert_eq!(stack.main_stack, v);
        assert_eq!(cond_stack.size, 1);
        assert_eq!(cond_stack.first_false_pos, None);
        // op_if([0], {0,None}) -> [], {1,0}
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(0));
        let mut cond_stack = ConditionStack::new();
        let v: Vec<StackEntry> = vec![];
        op_if(&mut stack, &mut cond_stack);
        assert_eq!(stack.main_stack, v);
        assert_eq!(cond_stack.size, 1);
        assert_eq!(cond_stack.first_false_pos, Some(0));
        // op_if([1], {1,0}) -> [1], {2,0}
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        let mut cond_stack = ConditionStack::new();
        cond_stack.size = 1;
        cond_stack.first_false_pos = Some(0);
        let v: Vec<StackEntry> = vec![StackEntry::Num(1)];
        op_if(&mut stack, &mut cond_stack);
        assert_eq!(stack.main_stack, v);
        assert_eq!(cond_stack.size, 2);
        assert_eq!(cond_stack.first_false_pos, Some(0));
        // error item type
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes(String::new()));
        let mut cond_stack = ConditionStack::new();
        let b = op_if(&mut stack, &mut cond_stack);
        assert!(!b);
        // error num items
        let mut stack = Stack::new();
        let mut cond_stack = ConditionStack::new();
        let b = op_if(&mut stack, &mut cond_stack);
        assert!(!b)
    }

    #[test]
    // Test OP_NOTIF
    fn test_notif() {
        // op_notif([0], {0,None}) -> [], {1,None}
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(0));
        let mut cond_stack = ConditionStack::new();
        let v: Vec<StackEntry> = vec![];
        op_notif(&mut stack, &mut cond_stack);
        assert_eq!(stack.main_stack, v);
        assert_eq!(cond_stack.size, 1);
        assert_eq!(cond_stack.first_false_pos, None);
        // op_notif([1], {0,None}) -> [], {1,0}
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        let mut cond_stack = ConditionStack::new();
        let v: Vec<StackEntry> = vec![];
        op_notif(&mut stack, &mut cond_stack);
        assert_eq!(stack.main_stack, v);
        assert_eq!(cond_stack.size, 1);
        assert_eq!(cond_stack.first_false_pos, Some(0));
        // op_notif([0], {1,0}) -> [0], {2,0}
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(0));
        let mut cond_stack = ConditionStack::new();
        cond_stack.size = 1;
        cond_stack.first_false_pos = Some(0);
        let v: Vec<StackEntry> = vec![StackEntry::Num(0)];
        op_notif(&mut stack, &mut cond_stack);
        assert_eq!(stack.main_stack, v);
        assert_eq!(cond_stack.size, 2);
        assert_eq!(cond_stack.first_false_pos, Some(0));
        // error item type
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes(String::new()));
        let mut cond_stack = ConditionStack::new();
        let b = op_notif(&mut stack, &mut cond_stack);
        assert!(!b);
        // error num items
        let mut stack = Stack::new();
        let mut cond_stack = ConditionStack::new();
        let b = op_notif(&mut stack, &mut cond_stack);
        assert!(!b)
    }

    #[test]
    // Test OP_ELSE
    fn test_else() {
        // op_else({1,None}) -> {1,0}
        let mut cond_stack = ConditionStack::new();
        cond_stack.size = 1;
        cond_stack.first_false_pos = None;
        op_else(&mut cond_stack);
        assert_eq!(cond_stack.size, 1);
        assert_eq!(cond_stack.first_false_pos, Some(0));
        // op_else({1,0}) -> {1,None}
        let mut cond_stack = ConditionStack::new();
        cond_stack.size = 1;
        cond_stack.first_false_pos = Some(0);
        op_else(&mut cond_stack);
        assert_eq!(cond_stack.size, 1);
        assert_eq!(cond_stack.first_false_pos, None);
        // op_else({2,0}) -> {2,0}
        let mut cond_stack = ConditionStack::new();
        cond_stack.size = 2;
        cond_stack.first_false_pos = Some(0);
        op_else(&mut cond_stack);
        assert_eq!(cond_stack.size, 2);
        assert_eq!(cond_stack.first_false_pos, Some(0));
        // empty condition stack
        let mut cond_stack = ConditionStack::new();
        let b = op_else(&mut cond_stack);
        assert!(!b)
    }

    #[test]
    // Test OP_ENDIF
    fn test_endif() {
        // op_endif({1,None}) -> {0,None}
        let mut cond_stack = ConditionStack::new();
        cond_stack.size = 1;
        cond_stack.first_false_pos = None;
        op_endif(&mut cond_stack);
        assert_eq!(cond_stack.size, 0);
        assert_eq!(cond_stack.first_false_pos, None);
        // op_endif({1,0}) -> {0,None}
        let mut cond_stack = ConditionStack::new();
        cond_stack.size = 1;
        cond_stack.first_false_pos = Some(0);
        op_endif(&mut cond_stack);
        assert_eq!(cond_stack.size, 0);
        assert_eq!(cond_stack.first_false_pos, None);
        // op_endif({2,0}) -> {1,0}
        let mut cond_stack = ConditionStack::new();
        cond_stack.size = 2;
        cond_stack.first_false_pos = Some(0);
        op_endif(&mut cond_stack);
        assert_eq!(cond_stack.size, 1);
        assert_eq!(cond_stack.first_false_pos, Some(0));
        // empty condition stack
        let mut cond_stack = ConditionStack::new();
        let b = op_endif(&mut cond_stack);
        assert!(!b)
    }

    #[test]
    // Test OP_VERIFY
    fn test_verify() {
        // op_verify([1]) -> []
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        let v: Vec<StackEntry> = vec![];
        op_verify(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_verify([0]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(0));
        let b = op_verify(&mut stack);
        assert!(!b);
        // op_verify([]) -> fail
        let mut stack = Stack::new();
        let b = op_verify(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_BURN
    fn test_burn() {
        // op_burn([1]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        let b = op_burn(&mut stack);
        assert!(!b);
        // op_burn([]) -> fail
        let mut stack = Stack::new();
        let b = op_burn(&mut stack);
        assert!(!b)
    }

    /*---- STACK OPS ----*/

    #[test]
    // Test OP_TOALTSTACK
    fn test_toaltstack() {
        // op_toaltstack([1], []) -> [], [1]
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        let v1: Vec<StackEntry> = vec![];
        let v2: Vec<StackEntry> = vec![StackEntry::Num(1)];
        op_toaltstack(&mut stack);
        assert_eq!(stack.main_stack, v1);
        assert_eq!(stack.alt_stack, v2);
        // op_toaltstack([], []) -> fail
        let mut stack = Stack::new();
        let b = op_toaltstack(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_FROMALTSTACK
    fn test_fromaltstack() {
        // op_fromaltstack([], [1]) -> [1], []
        let mut stack = Stack::new();
        stack.alt_stack.push(StackEntry::Num(1));
        let v1: Vec<StackEntry> = vec![StackEntry::Num(1)];
        let v2: Vec<StackEntry> = vec![];
        op_fromaltstack(&mut stack);
        assert_eq!(stack.main_stack, v1);
        assert_eq!(stack.alt_stack, v2);
        // op_fromaltstack([], []) -> fail
        let mut stack = Stack::new();
        let b = op_fromaltstack(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_2DROP
    fn test_2drop() {
        // op_2drop([1,2]) -> []
        let mut stack = Stack::new();
        for i in 1..=2 {
            stack.push(StackEntry::Num(i));
        }
        let v: Vec<StackEntry> = vec![];
        op_2drop(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_2drop([1]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        let b = op_2drop(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_2DUP
    fn test_2dup() {
        // op_2dup([1,2]) -> [1,2,1,2]
        let mut stack = Stack::new();
        for i in 1..=2 {
            stack.push(StackEntry::Num(i));
        }
        let mut v: Vec<StackEntry> = vec![];
        for i in 1..=2 {
            v.push(StackEntry::Num(i));
        }
        for i in 1..=2 {
            v.push(StackEntry::Num(i));
        }
        op_2dup(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_2dup([1]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        let b = op_2dup(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_3DUP
    fn test_3dup() {
        // op_3dup([1,2,3]) -> [1,2,3,1,2,3]
        let mut stack = Stack::new();
        for i in 1..=3 {
            stack.push(StackEntry::Num(i));
        }
        let mut v: Vec<StackEntry> = vec![];
        for i in 1..=3 {
            v.push(StackEntry::Num(i));
        }
        for i in 1..=3 {
            v.push(StackEntry::Num(i));
        }
        op_3dup(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_3dup([1,2]) -> fail
        let mut stack = Stack::new();
        for i in 1..=2 {
            stack.push(StackEntry::Num(i));
        }
        let b = op_3dup(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_2OVER
    fn test_2over() {
        // op_2over([1,2,3,4]) -> [1,2,3,4,1,2]
        let mut stack = Stack::new();
        for i in 1..=4 {
            stack.push(StackEntry::Num(i));
        }
        let mut v: Vec<StackEntry> = vec![];
        for i in 1..=4 {
            v.push(StackEntry::Num(i));
        }
        for i in 1..=2 {
            v.push(StackEntry::Num(i));
        }
        op_2over(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_2over([1,2,3]) -> fail
        let mut stack = Stack::new();
        for i in 1..=3 {
            stack.push(StackEntry::Num(i));
        }
        let b = op_2over(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_2ROT
    fn test_2rot() {
        // op_2rot([1,2,3,4,5,6]) -> [3,4,5,6,1,2]
        let mut stack = Stack::new();
        for i in 1..=6 {
            stack.push(StackEntry::Num(i));
        }
        let mut v: Vec<StackEntry> = vec![];
        for i in 3..=6 {
            v.push(StackEntry::Num(i));
        }
        for i in 1..=2 {
            v.push(StackEntry::Num(i));
        }
        op_2rot(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_2rot([1,2,3,4,5]) -> fail
        let mut stack = Stack::new();
        for i in 1..=5 {
            stack.push(StackEntry::Num(i));
        }
        let b = op_2rot(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_2SWAP
    fn test_2swap() {
        // op_2swap([1,2,3,4]) -> [3,4,1,2]
        let mut stack = Stack::new();
        for i in 1..=4 {
            stack.push(StackEntry::Num(i));
        }
        let mut v: Vec<StackEntry> = vec![];
        for i in 3..=4 {
            v.push(StackEntry::Num(i));
        }
        for i in 1..=2 {
            v.push(StackEntry::Num(i));
        }
        op_2swap(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_2swap([1,2,3]) -> fail
        let mut stack = Stack::new();
        for i in 1..=3 {
            stack.push(StackEntry::Num(i));
        }
        let b = op_2swap(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_IFDUP
    fn test_ifdup() {
        // op_ifdup([1]) -> [1,1]
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        let mut v: Vec<StackEntry> = vec![];
        for _i in 1..=2 {
            v.push(StackEntry::Num(1));
        }
        op_ifdup(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_ifdup([0]) -> [0]
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(0));
        let v: Vec<StackEntry> = vec![StackEntry::Num(0)];
        op_ifdup(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_ifdup([]) -> fail
        let mut stack = Stack::new();
        let b = op_ifdup(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_DEPTH
    fn test_depth() {
        // op_depth([1,1,1,1]) -> [1,1,1,1,4]
        let mut stack = Stack::new();
        for _i in 1..=4 {
            stack.push(StackEntry::Num(1));
        }
        let mut v: Vec<StackEntry> = vec![];
        for _i in 1..=4 {
            v.push(StackEntry::Num(1));
        }
        v.push(StackEntry::Num(4));
        op_depth(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_depth([]) -> [0]
        let mut stack = Stack::new();
        let v: Vec<StackEntry> = vec![StackEntry::Num(0)];
        op_depth(&mut stack);
        assert_eq!(stack.main_stack, v)
    }

    #[test]
    // Test OP_DROP
    fn test_drop() {
        // op_drop([1]) -> []
        let mut stack = Stack::new();
        let _v: Vec<StackEntry> = vec![StackEntry::Num(1)];
        let v: Vec<StackEntry> = vec![];
        op_drop(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_drop([]) -> fail
        let mut stack = Stack::new();
        let b = op_drop(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_DUP
    fn test_dup() {
        // op_dup([1]) -> [1,1]
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        let mut v: Vec<StackEntry> = vec![];
        for _i in 1..=2 {
            v.push(StackEntry::Num(1));
        }
        op_dup(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_dup([]) -> fail
        let mut stack = Stack::new();
        let b = op_dup(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_NIP
    fn test_nip() {
        // op_nip([1,2]) -> [2]
        let mut stack = Stack::new();
        for i in 1..=2 {
            stack.push(StackEntry::Num(i));
        }
        let v: Vec<StackEntry> = vec![StackEntry::Num(2)];
        op_nip(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_nip([1]) -> fail
        let mut stack = Stack::new();
        let _v: Vec<StackEntry> = vec![StackEntry::Num(1)];
        let b = op_nip(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_OVER
    fn test_over() {
        // op_over([1,2]) -> [1,2,1]
        let mut stack = Stack::new();
        for i in 1..=2 {
            stack.push(StackEntry::Num(i));
        }
        let mut v: Vec<StackEntry> = vec![];
        for i in 1..=2 {
            v.push(StackEntry::Num(i));
        }
        v.push(StackEntry::Num(1));
        op_over(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_over([1]) -> fail
        let mut stack = Stack::new();
        let _v: Vec<StackEntry> = vec![StackEntry::Num(1)];
        let b = op_over(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_PICK
    fn test_pick() {
        // op_pick([1,2,3,4,3]) -> [1,2,3,4,1]
        let mut stack = Stack::new();
        for i in 1..=4 {
            stack.push(StackEntry::Num(i));
        }
        stack.push(StackEntry::Num(3));
        let mut v: Vec<StackEntry> = vec![];
        for i in 1..=4 {
            v.push(StackEntry::Num(i));
        }
        v.push(StackEntry::Num(1));
        op_pick(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_pick([1,2,3,4,0]) -> [1,2,3,4,4]
        let mut stack = Stack::new();
        for i in 1..=4 {
            stack.push(StackEntry::Num(i));
        }
        stack.push(StackEntry::Num(0));
        let mut v: Vec<StackEntry> = vec![];
        for i in 1..=4 {
            v.push(StackEntry::Num(i));
        }
        v.push(StackEntry::Num(4));
        op_pick(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_pick([1]) -> fail
        let mut stack = Stack::new();
        let _v: Vec<StackEntry> = vec![StackEntry::Num(1)];
        let b = op_pick(&mut stack);
        assert!(!b);
        // op_pick([1,"hello"]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        stack.push(StackEntry::Bytes("hello".to_string()));
        let b = op_pick(&mut stack);
        assert!(!b);
        // op_pick([1,1]) -> fail
        let mut stack = Stack::new();
        for i in 1..=2 {
            stack.push(StackEntry::Num(i));
        }
        let b = op_pick(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_ROLL
    fn test_roll() {
        // op_roll([1,2,3,4,3]) -> [2,3,4,1]
        let mut stack = Stack::new();
        for i in 1..=4 {
            stack.push(StackEntry::Num(i));
        }
        stack.push(StackEntry::Num(3));
        let mut v: Vec<StackEntry> = vec![];
        for i in 2..=4 {
            v.push(StackEntry::Num(i));
        }
        v.push(StackEntry::Num(1));
        op_roll(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_roll([1,2,3,4,0]) -> [1,2,3,4]
        let mut stack = Stack::new();
        for i in 1..=4 {
            stack.push(StackEntry::Num(i));
        }
        stack.push(StackEntry::Num(0));
        let mut v: Vec<StackEntry> = vec![];
        for i in 1..=4 {
            v.push(StackEntry::Num(i));
        }
        op_roll(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_roll([1]) -> fail
        let mut stack = Stack::new();
        let _v: Vec<StackEntry> = vec![StackEntry::Num(1)];
        let b = op_roll(&mut stack);
        assert!(!b);
        // op_roll([1,"hello"]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        stack.push(StackEntry::Bytes("hello".to_string()));
        let b = op_roll(&mut stack);
        assert!(!b);
        // op_roll([1,1]) -> fail
        let mut stack = Stack::new();
        for i in 1..=2 {
            stack.push(StackEntry::Num(i));
        }
        let b = op_roll(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_ROT
    fn test_rot() {
        // op_rot([1,2,3]) -> [2,3,1]
        let mut stack = Stack::new();
        for i in 1..=3 {
            stack.push(StackEntry::Num(i));
        }
        let mut v: Vec<StackEntry> = vec![];
        for i in 2..=3 {
            v.push(StackEntry::Num(i));
        }
        v.push(StackEntry::Num(1));
        op_rot(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_rot([1,2]) -> fail
        let mut stack = Stack::new();
        for i in 1..=2 {
            stack.push(StackEntry::Num(i));
        }
        let b = op_rot(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_SWAP
    fn test_swap() {
        // op_swap([1,2]) -> [2,1]
        let mut stack = Stack::new();
        for i in 1..=2 {
            stack.push(StackEntry::Num(i));
        }
        let v: Vec<StackEntry> = vec![StackEntry::Num(2), StackEntry::Num(1)];
        op_swap(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_swap([1]) -> fail
        let mut stack = Stack::new();
        let _v: Vec<StackEntry> = vec![StackEntry::Num(1)];
        let b = op_swap(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_TUCK
    fn test_tuck() {
        // op_tuck([1,2]) -> [2,1,2]
        let mut stack = Stack::new();
        for i in 1..=2 {
            stack.push(StackEntry::Num(i));
        }
        let mut v: Vec<StackEntry> = vec![StackEntry::Num(2)];
        for i in 1..=2 {
            v.push(StackEntry::Num(i));
        }
        op_tuck(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_tuck([1]) -> fail
        let mut stack = Stack::new();
        let _v: Vec<StackEntry> = vec![StackEntry::Num(1)];
        let b = op_tuck(&mut stack);
        assert!(!b)
    }

    /*---- SPLICE OPS ----*/

    #[test]
    // Test OP_CAT
    fn test_cat() {
        // op_cat(["hello","world"]) -> ["helloworld"]
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes("hello".to_string()));
        stack.push(StackEntry::Bytes("world".to_string()));
        let v: Vec<StackEntry> = vec![StackEntry::Bytes("helloworld".to_string())];
        op_cat(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_cat(["hello",""]) -> ["hello"]
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes("hello".to_string()));
        stack.push(StackEntry::Bytes("".to_string()));
        let v: Vec<StackEntry> = vec![StackEntry::Bytes("hello".to_string())];
        op_cat(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_cat(["a","a"*MAX_SCRIPT_ITEM_SIZE]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes("a".to_string()));
        let mut s = String::new();
        for _i in 1..=MAX_SCRIPT_ITEM_SIZE {
            s.push('a');
        }
        stack.push(StackEntry::Bytes(s.to_string()));
        let b = op_cat(&mut stack);
        assert!(!b);
        // op_cat(["hello"]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes("hello".to_string()));
        let b = op_cat(&mut stack);
        assert!(!b);
        // op_cat(["hello", 1]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes("hello".to_string()));
        stack.push(StackEntry::Num(1));
        let b = op_cat(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_SUBSTR
    fn test_substr() {
        // op_substr(["hello",1,2]) -> ["el"]
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes("hello".to_string()));
        for i in 1..=2 {
            stack.push(StackEntry::Num(i));
        }
        let v: Vec<StackEntry> = vec![StackEntry::Bytes("el".to_string())];
        op_substr(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_substr(["hello",0,0]) -> [""]
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes("hello".to_string()));
        for _i in 1..=2 {
            stack.push(StackEntry::Num(0));
        }
        let v: Vec<StackEntry> = vec![StackEntry::Bytes("".to_string())];
        op_substr(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_substr(["hello",0,5]) -> ["hello"]
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes("hello".to_string()));
        stack.push(StackEntry::Num(0));
        stack.push(StackEntry::Num(5));
        let v: Vec<StackEntry> = vec![StackEntry::Bytes("hello".to_string())];
        op_substr(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_substr(["hello",5,0]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes("hello".to_string()));
        stack.push(StackEntry::Num(5));
        stack.push(StackEntry::Num(0));
        let b = op_substr(&mut stack);
        assert!(!b);
        // op_substr(["hello",1,5]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes("hello".to_string()));
        stack.push(StackEntry::Num(1));
        stack.push(StackEntry::Num(5));
        let b = op_substr(&mut stack);
        assert!(!b);
        // op_substr(["hello",1]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes("hello".to_string()));
        stack.push(StackEntry::Num(1));
        let b = op_substr(&mut stack);
        assert!(!b);
        // op_substr(["hello",1,usize::MAX]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes("hello".to_string()));
        stack.push(StackEntry::Num(1));
        stack.push(StackEntry::Num(usize::MAX));
        let b = op_substr(&mut stack);
        assert!(!b);
        // op_substr(["hello",1,""]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes("hello".to_string()));
        stack.push(StackEntry::Num(1));
        stack.push(StackEntry::Bytes("".to_string()));
        let b = op_substr(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_LEFT
    fn test_left() {
        // op_left(["hello",2]) -> ["he"]
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes("hello".to_string()));
        stack.push(StackEntry::Num(2));
        let v: Vec<StackEntry> = vec![StackEntry::Bytes("he".to_string())];
        op_left(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_left(["hello",0]) -> [""]
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes("hello".to_string()));
        stack.push(StackEntry::Num(0));
        let v: Vec<StackEntry> = vec![StackEntry::Bytes("".to_string())];
        op_left(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_left(["hello",5]) -> ["hello"]
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes("hello".to_string()));
        stack.push(StackEntry::Num(5));
        let v: Vec<StackEntry> = vec![StackEntry::Bytes("hello".to_string())];
        op_left(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_left(["hello",""]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes("hello".to_string()));
        stack.push(StackEntry::Bytes("".to_string()));
        let b = op_left(&mut stack);
        assert!(!b);
        // op_left(["hello"]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes("hello".to_string()));
        let b = op_left(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_RIGHT
    fn test_right() {
        // op_right(["hello",0]) -> ["hello"]
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes("hello".to_string()));
        stack.push(StackEntry::Num(0));
        let v: Vec<StackEntry> = vec![StackEntry::Bytes("hello".to_string())];
        op_right(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_right(["hello",2]) -> ["llo"]
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes("hello".to_string()));
        stack.push(StackEntry::Num(2));
        let v: Vec<StackEntry> = vec![StackEntry::Bytes("llo".to_string())];
        op_right(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_right(["hello",5]) -> [""]
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes("hello".to_string()));
        stack.push(StackEntry::Num(5));
        let v: Vec<StackEntry> = vec![StackEntry::Bytes("".to_string())];
        op_right(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_right(["hello",""]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes("hello".to_string()));
        stack.push(StackEntry::Bytes("".to_string()));
        let b = op_right(&mut stack);
        assert!(!b);
        // op_right(["hello"]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes("hello".to_string()));
        let b = op_right(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_SIZE
    fn test_size() {
        // op_size(["hello"]) -> ["hello",5]
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes("hello".to_string()));
        let v: Vec<StackEntry> = vec![StackEntry::Bytes("hello".to_string()), StackEntry::Num(5)];
        op_size(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_size([""]) -> ["",0]
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes("".to_string()));
        let v: Vec<StackEntry> = vec![StackEntry::Bytes("".to_string()), StackEntry::Num(0)];
        op_size(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_size([1]) -> fail
        let mut stack = Stack::new();
        let _v: Vec<StackEntry> = vec![StackEntry::Num(1)];
        let b = op_size(&mut stack);
        assert!(!b);
        // op_size([]) -> fail
        let mut stack = Stack::new();
        let b = op_size(&mut stack);
        assert!(!b)
    }

    /*---- BITWISE LOGIC OPS ----*/

    #[test]
    // Test OP_INVERT
    fn test_invert() {
        // op_invert([0]) -> [usize::MAX]
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(0));
        let v: Vec<StackEntry> = vec![StackEntry::Num(usize::MAX)];
        op_invert(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_invert([]) -> fail
        let mut stack = Stack::new();
        let b = op_invert(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_AND
    fn test_and() {
        // op_and([1,2]) -> [0]
        let mut stack = Stack::new();
        for i in 1..=2 {
            stack.push(StackEntry::Num(i));
        }
        let v: Vec<StackEntry> = vec![StackEntry::Num(0)];
        op_and(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_and([1]) -> fail
        let mut stack = Stack::new();
        let _v: Vec<StackEntry> = vec![StackEntry::Num(1)];
        let b = op_and(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_OR
    fn test_or() {
        // op_or([1,2]) -> [3]
        let mut stack = Stack::new();
        for i in 1..=2 {
            stack.push(StackEntry::Num(i));
        }
        let v: Vec<StackEntry> = vec![StackEntry::Num(3)];
        op_or(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_or([1]) -> fail
        let mut stack = Stack::new();
        let _v: Vec<StackEntry> = vec![StackEntry::Num(1)];
        let b = op_or(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_XOR
    fn test_xor() {
        // op_xor([1,2]) -> [3]
        let mut stack = Stack::new();
        for i in 1..=2 {
            stack.push(StackEntry::Num(i));
        }
        let v: Vec<StackEntry> = vec![StackEntry::Num(3)];
        op_xor(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_xor([1,1]) -> [0]
        let mut stack = Stack::new();
        for _i in 1..=2 {
            stack.push(StackEntry::Num(1));
        }
        let v: Vec<StackEntry> = vec![StackEntry::Num(0)];
        op_xor(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_xor([1]) -> fail
        let mut stack = Stack::new();
        let _v: Vec<StackEntry> = vec![StackEntry::Num(1)];
        let b = op_xor(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_EQUAL
    fn test_equal() {
        // op_equal(["hello","hello"]) -> [1]
        let mut stack = Stack::new();
        for _i in 1..=2 {
            stack.push(StackEntry::Bytes("hello".to_string()));
        }
        let v: Vec<StackEntry> = vec![StackEntry::Num(1)];
        op_equal(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_equal([1,2]) -> [0]
        let mut stack = Stack::new();
        for i in 1..=2 {
            stack.push(StackEntry::Num(i));
        }
        let v: Vec<StackEntry> = vec![StackEntry::Num(0)];
        op_equal(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_equal([1]) -> fail
        let mut stack = Stack::new();
        let _v: Vec<StackEntry> = vec![StackEntry::Num(1)];
        let b = op_equal(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_EQUALVERIFY
    fn test_equalverify() {
        // op_equalverify(["hello","hello"]) -> []
        let mut stack = Stack::new();
        for _i in 1..=2 {
            stack.push(StackEntry::Bytes("hello".to_string()));
        }
        let v: Vec<StackEntry> = vec![];
        op_equalverify(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_equalverify([1,2]) -> fail
        let mut stack = Stack::new();
        for i in 1..=2 {
            stack.push(StackEntry::Num(i));
        }
        let b = op_equalverify(&mut stack);
        assert!(!b);
        // op_equalverify([1]) -> fail
        let mut stack = Stack::new();
        let _v: Vec<StackEntry> = vec![StackEntry::Num(1)];
        let b = op_equalverify(&mut stack);
        assert!(!b)
    }

    /*---- ARITHMETIC OPS ----*/

    #[test]
    // Test OP_1ADD
    fn test_1add() {
        // op_1add([1]) -> [2]
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        let v: Vec<StackEntry> = vec![StackEntry::Num(2)];
        op_1add(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_1add([usize::MAX]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(usize::MAX));
        let b = op_1add(&mut stack);
        assert!(!b);
        // op_1add([]) -> fail
        let mut stack = Stack::new();
        let b = op_1add(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_1SUB
    fn test_1sub() {
        // op_1sub([1]) -> [0]
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        let v: Vec<StackEntry> = vec![StackEntry::Num(0)];
        op_1sub(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_1sub([0]) -> fail
        let mut stack = Stack::new();
        let _v: Vec<StackEntry> = vec![StackEntry::Num(0)];
        let b = op_1sub(&mut stack);
        assert!(!b);
        // op_1sub([]) -> fail
        let mut stack = Stack::new();
        let b = op_1sub(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_2MUL
    fn test_2mul() {
        // op_2mul([1]) -> [2]
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        let v: Vec<StackEntry> = vec![StackEntry::Num(2)];
        op_2mul(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_2mul([usize::MAX]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(usize::MAX));
        let b = op_2mul(&mut stack);
        assert!(!b);
        // op_2mul([]) -> fail
        let mut stack = Stack::new();
        let b = op_2mul(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_2DIV
    fn test_2div() {
        // op_2div([1]) -> [0]
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        let v: Vec<StackEntry> = vec![StackEntry::Num(0)];
        op_2div(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_2div([]) -> fail
        let mut stack = Stack::new();
        let b = op_2div(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_NOT
    fn test_not() {
        // op_not([0]) -> [1]
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(0));
        let v: Vec<StackEntry> = vec![StackEntry::Num(1)];
        op_not(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_not([1]) -> [0]
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        let v: Vec<StackEntry> = vec![StackEntry::Num(0)];
        op_not(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_not([]) -> fail
        let mut stack = Stack::new();
        let b = op_not(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_0NOTEQUAL
    fn test_0notequal() {
        // op_0notequal([1]) -> [1]
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        let v: Vec<StackEntry> = vec![StackEntry::Num(1)];
        op_0notequal(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_0notequal([0]) -> [0]
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(0));
        let v: Vec<StackEntry> = vec![StackEntry::Num(0)];
        op_0notequal(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_0notequal([]) -> fail
        let mut stack = Stack::new();
        let b = op_0notequal(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_ADD
    fn test_add() {
        // op_add([1,2]) -> [3]
        let mut stack = Stack::new();
        for i in 1..=2 {
            stack.push(StackEntry::Num(i));
        }
        let v: Vec<StackEntry> = vec![StackEntry::Num(3)];
        op_add(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_add([1,usize::MAX]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        stack.push(StackEntry::Num(usize::MAX));
        let b = op_add(&mut stack);
        assert!(!b);
        // op_add([1]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        let b = op_add(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_SUB
    fn test_sub() {
        // op_sub([1,0]) -> [1]
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        stack.push(StackEntry::Num(0));
        let v: Vec<StackEntry> = vec![StackEntry::Num(1)];
        op_sub(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_sub([0,1]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(0));
        stack.push(StackEntry::Num(1));
        let b = op_sub(&mut stack);
        assert!(!b);
        // op_sub([1]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        let b = op_sub(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_MUL
    fn test_mul() {
        // op_mul([1,2]) -> [2]
        let mut stack = Stack::new();
        for i in 1..=2 {
            stack.push(StackEntry::Num(i));
        }
        let v: Vec<StackEntry> = vec![StackEntry::Num(2)];
        op_mul(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_mul([2,usize::MAX]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(2));
        stack.push(StackEntry::Num(usize::MAX));
        let b = op_mul(&mut stack);
        assert!(!b);
        // op_mul([1]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        let b = op_mul(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_DIV
    fn test_div() {
        // op_div([1,2]) -> [0]
        let mut stack = Stack::new();
        for i in 1..=2 {
            stack.push(StackEntry::Num(i));
        }
        let v: Vec<StackEntry> = vec![StackEntry::Num(0)];
        op_div(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_div([1,0]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        stack.push(StackEntry::Num(0));
        let b = op_div(&mut stack);
        assert!(!b);
        // op_div([1]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        let b = op_div(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_MOD
    fn test_mod() {
        // op_mod([1,2]) -> [1]
        let mut stack = Stack::new();
        for i in 1..=2 {
            stack.push(StackEntry::Num(i));
        }
        let v: Vec<StackEntry> = vec![StackEntry::Num(1)];
        op_mod(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_mod([1,0]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        stack.push(StackEntry::Num(0));
        let b = op_mod(&mut stack);
        assert!(!b);
        // op_mod([1]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        let b = op_mod(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_LSHIFT
    fn test_lshift() {
        // op_lshift([1,2]) -> [4]
        let mut stack = Stack::new();
        for i in 1..=2 {
            stack.push(StackEntry::Num(i));
        }
        let v: Vec<StackEntry> = vec![StackEntry::Num(4)];
        op_lshift(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_lshift([1,64]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        stack.push(StackEntry::Num(64));
        let b = op_lshift(&mut stack);
        assert!(!b);
        // op_lshift([1]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        let b = op_lshift(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_RSHIFT
    fn test_rshift() {
        // op_rshift([1,2]) -> [0]
        let mut stack = Stack::new();
        for i in 1..=2 {
            stack.push(StackEntry::Num(i));
        }
        let v: Vec<StackEntry> = vec![StackEntry::Num(0)];
        op_rshift(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_rshift([1,64]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        stack.push(StackEntry::Num(64));
        let b = op_rshift(&mut stack);
        assert!(!b);
        // op_rshift([1]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        let b = op_rshift(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_BOOLAND
    fn test_booland() {
        // op_booland([1,2]) -> [1]
        let mut stack = Stack::new();
        for i in 1..=2 {
            stack.push(StackEntry::Num(i));
        }
        let v: Vec<StackEntry> = vec![StackEntry::Num(1)];
        op_booland(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_booland([0,1]) -> [0]
        let mut stack = Stack::new();
        for i in 0..=1 {
            stack.push(StackEntry::Num(i));
        }
        let v: Vec<StackEntry> = vec![StackEntry::Num(0)];
        op_booland(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_booland([1]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        let b = op_booland(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_BOOLOR
    fn test_boolor() {
        // op_boolor([0,1]) -> [1]
        let mut stack = Stack::new();
        for i in 0..=1 {
            stack.push(StackEntry::Num(i));
        }
        let v: Vec<StackEntry> = vec![StackEntry::Num(1)];
        op_boolor(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_boolor([0,0]) -> [0]
        let mut stack = Stack::new();
        for _i in 1..=2 {
            stack.push(StackEntry::Num(0));
        }
        let v: Vec<StackEntry> = vec![StackEntry::Num(0)];
        op_boolor(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_boolor([1]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        let b = op_boolor(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_NUMEQUAL
    fn test_numequal() {
        // op_numequal([1,1]) -> [1]
        let mut stack = Stack::new();
        for _i in 1..=2 {
            stack.push(StackEntry::Num(1));
        }
        let v: Vec<StackEntry> = vec![StackEntry::Num(1)];
        op_numequal(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_numequal([1,2]) -> [0]
        let mut stack = Stack::new();
        for i in 1..=2 {
            stack.push(StackEntry::Num(i));
        }
        let v: Vec<StackEntry> = vec![StackEntry::Num(0)];
        op_numequal(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_numequal([1]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        let b = op_numequal(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_NUMEQUALVERIFY
    fn test_numequalverify() {
        // op_numequalverify([1,1]) -> []
        let mut stack = Stack::new();
        for _i in 1..=2 {
            stack.push(StackEntry::Num(1));
        }
        let v: Vec<StackEntry> = vec![];
        op_numequalverify(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_numequalverify([1,2]) -> fail
        let mut stack = Stack::new();
        for i in 1..=2 {
            stack.push(StackEntry::Num(i));
        }
        let b = op_numequalverify(&mut stack);
        assert!(!b);
        // op_numequalverify([1]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        let b = op_numequalverify(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_NUMNOTEQUAL
    fn test_numnotequal() {
        // op_numnotequal([1,2]) -> [1]
        let mut stack = Stack::new();
        for i in 1..=2 {
            stack.push(StackEntry::Num(i));
        }
        let v: Vec<StackEntry> = vec![StackEntry::Num(1)];
        op_numnotequal(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_numnotequal([1,1]) -> [0]
        let mut stack = Stack::new();
        for _i in 1..=2 {
            stack.push(StackEntry::Num(1));
        }
        let v: Vec<StackEntry> = vec![StackEntry::Num(0)];
        op_numnotequal(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_numnotequal([1]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        let b = op_numnotequal(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_LESSTHAN
    fn test_lessthan() {
        // op_lessthan([1,2]) -> [1]
        let mut stack = Stack::new();
        for i in 1..=2 {
            stack.push(StackEntry::Num(i));
        }
        let v: Vec<StackEntry> = vec![StackEntry::Num(1)];
        op_lessthan(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_lessthan([1,1]) -> [0]
        let mut stack = Stack::new();
        for _i in 1..=2 {
            stack.push(StackEntry::Num(1));
        }
        let v: Vec<StackEntry> = vec![StackEntry::Num(0)];
        op_lessthan(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_lessthan([1]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        let b = op_lessthan(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_GREATERTHAN
    fn test_greaterthan() {
        // op_greaterthan([2,1]) -> [1]
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(2));
        stack.push(StackEntry::Num(1));
        let v: Vec<StackEntry> = vec![StackEntry::Num(1)];
        op_greaterthan(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_greaterthan([1,1]) -> [0]
        let mut stack = Stack::new();
        for _i in 1..=2 {
            stack.push(StackEntry::Num(1));
        }
        let v: Vec<StackEntry> = vec![StackEntry::Num(0)];
        op_greaterthan(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_greaterthan([1]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        let b = op_greaterthan(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_LESSTHANOREQUAL
    fn test_lessthanorequal() {
        // test_lessthanorequal([1,1]) -> [1]
        let mut stack = Stack::new();
        for _i in 1..=2 {
            stack.push(StackEntry::Num(1));
        }
        let v: Vec<StackEntry> = vec![StackEntry::Num(1)];
        op_lessthanorequal(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_lessthanorequal([2,1]) -> [0]
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(2));
        stack.push(StackEntry::Num(1));
        let v: Vec<StackEntry> = vec![StackEntry::Num(0)];
        op_lessthanorequal(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_lessthanorequal([1]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        let b = op_lessthanorequal(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_GREATERTHANOREQUAL
    fn test_greaterthanorequal() {
        // op_greaterthanorequal([1,1]) -> [1]
        let mut stack = Stack::new();
        for _i in 1..=2 {
            stack.push(StackEntry::Num(1));
        }
        let v: Vec<StackEntry> = vec![StackEntry::Num(1)];
        op_greaterthanorequal(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_greaterthanorequal([1,2]) -> [0]
        let mut stack = Stack::new();
        for i in 1..=2 {
            stack.push(StackEntry::Num(i));
        }
        let v: Vec<StackEntry> = vec![StackEntry::Num(0)];
        op_greaterthanorequal(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_greaterthanorequal([1]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        let b = op_greaterthanorequal(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_MIN
    fn test_min() {
        // op_min([1,2]) -> [1]
        let mut stack = Stack::new();
        for i in 1..=2 {
            stack.push(StackEntry::Num(i));
        }
        let v: Vec<StackEntry> = vec![StackEntry::Num(1)];
        op_min(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_min([1]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        let b = op_min(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_MAX
    fn test_max() {
        // op_max([1,2]) -> [2]
        let mut stack = Stack::new();
        for i in 1..=2 {
            stack.push(StackEntry::Num(i));
        }
        let v: Vec<StackEntry> = vec![StackEntry::Num(2)];
        op_max(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_max([1]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        let b = op_max(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_WITHIN
    fn test_within() {
        // op_within([2,1,3]) -> [1]
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(2));
        stack.push(StackEntry::Num(1));
        stack.push(StackEntry::Num(3));
        let v: Vec<StackEntry> = vec![StackEntry::Num(1)];
        op_within(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_within([1,2,3]) -> [0]
        let mut stack = Stack::new();
        for i in 1..=3 {
            stack.push(StackEntry::Num(i));
        }
        let v: Vec<StackEntry> = vec![StackEntry::Num(0)];
        op_within(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_within([1,2]) -> fail
        let mut stack = Stack::new();
        for i in 1..=2 {
            stack.push(StackEntry::Num(i));
        }
        let b = op_within(&mut stack);
        assert!(!b)
    }

    /*---- CRYPTO OPS ----*/

    #[test]
    // Test OP_SHA3
    fn test_sha3() {
        // op_sha3([sig]) -> [sha3_256(sig)]
        let (pk, sk) = sign::gen_keypair();
        let msg = hex::encode(vec![0, 0, 0]);
        let sig = sign::sign_detached(msg.as_bytes(), &sk);
        let h = hex::encode(sha3_256::digest(sig.as_ref()));
        let mut stack = Stack::new();
        stack.push(StackEntry::Signature(sig));
        let v: Vec<StackEntry> = vec![StackEntry::Bytes(h)];
        op_sha3(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_sha3([pk]) -> [sha3_256(pk)]
        let h = hex::encode(sha3_256::digest(pk.as_ref()));
        let mut stack = Stack::new();
        stack.push(StackEntry::PubKey(pk));
        let v: Vec<StackEntry> = vec![StackEntry::Bytes(h)];
        op_sha3(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_sha3(["hello"]) -> [sha3_256("hello")]
        let s = "hello".to_string();
        let h = hex::encode(sha3_256::digest(s.as_bytes()));
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes(s));
        let v: Vec<StackEntry> = vec![StackEntry::Bytes(h)];
        op_sha3(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_sha3([1]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(1));
        let b = op_sha3(&mut stack);
        assert!(!b);
        // op_sha3([]) -> fail
        let mut stack = Stack::new();
        let b = op_sha3(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_HASH256
    fn test_hash256() {
        // op_hash256([pk]) -> [addr]
        let (pk, _sk) = sign::gen_keypair();
        let mut stack = Stack::new();
        stack.push(StackEntry::PubKey(pk));
        let v: Vec<StackEntry> = vec![StackEntry::Bytes(construct_address(&pk))];
        op_hash256(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_hash256([]) -> fail
        let mut stack = Stack::new();
        let b = op_hash256(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_HASH256_V0
    fn test_hash256_v0() {
        // op_hash256_v0([pk]) -> [addr_v0]
        let (pk, _sk) = sign::gen_keypair();
        let mut stack = Stack::new();
        stack.push(StackEntry::PubKey(pk));
        let v: Vec<StackEntry> = vec![StackEntry::Bytes(construct_address_v0(&pk))];
        op_hash256_v0(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_hash256([]) -> fail
        let mut stack = Stack::new();
        let b = op_hash256_v0(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_HASH256_TEMP
    fn test_hash256_temp() {
        // op_hash256_temp([pk]) -> [addr_temp]
        let (pk, _sk) = sign::gen_keypair();
        let mut stack = Stack::new();
        stack.push(StackEntry::PubKey(pk));
        let v: Vec<StackEntry> = vec![StackEntry::Bytes(construct_address_temp(&pk))];
        op_hash256_temp(&mut stack);
        assert_eq!(stack.main_stack, v);
        // op_hash256([]) -> fail
        let mut stack = Stack::new();
        let b = op_hash256_temp(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_CHECKSIG
    fn test_checksig() {
        // op_checksig([msg,sig,pk]) -> [1]
        let (pk, sk) = sign::gen_keypair();
        let msg = hex::encode(vec![0, 0, 0]);
        let sig = sign::sign_detached(msg.as_bytes(), &sk);
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes(msg));
        stack.push(StackEntry::Signature(sig));
        stack.push(StackEntry::PubKey(pk));
        let v: Vec<StackEntry> = vec![StackEntry::Num(1)];
        op_checksig(&mut stack);
        assert_eq!(stack.main_stack, v);
        // wrong message
        // op_checksig([msg',sig,pk]) -> [0]
        let msg = hex::encode(vec![0, 0, 1]);
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes(msg));
        stack.push(StackEntry::Signature(sig));
        stack.push(StackEntry::PubKey(pk));
        let v: Vec<StackEntry> = vec![StackEntry::Num(0)];
        op_checksig(&mut stack);
        assert_eq!(stack.main_stack, v);
        // wrong public key
        // op_checksig([msg,sig,pk']) -> [0]
        let (pk, _sk) = sign::gen_keypair();
        let msg = hex::encode(vec![0, 0, 0]);
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes(msg));
        stack.push(StackEntry::Signature(sig));
        stack.push(StackEntry::PubKey(pk));
        let v: Vec<StackEntry> = vec![StackEntry::Num(0)];
        op_checksig(&mut stack);
        assert_eq!(stack.main_stack, v);
        // no message
        // op_checksig([sig,pk]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Signature(sig));
        stack.push(StackEntry::PubKey(pk));
        let b = op_checksig(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_CHECKSIGVERIFY
    fn test_checksigverify() {
        // op_checksigverify([msg,sig,pk]) -> []
        let (pk, sk) = sign::gen_keypair();
        let msg = hex::encode(vec![0, 0, 0]);
        let sig = sign::sign_detached(msg.as_bytes(), &sk);
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes(msg));
        stack.push(StackEntry::Signature(sig));
        stack.push(StackEntry::PubKey(pk));
        let v: Vec<StackEntry> = vec![];
        op_checksigverify(&mut stack);
        assert_eq!(stack.main_stack, v);
        // wrong message
        // op_checksigverify([msg',sig,pk]) -> fail
        let msg = hex::encode(vec![0, 0, 1]);
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes(msg));
        stack.push(StackEntry::Signature(sig));
        stack.push(StackEntry::PubKey(pk));
        let b = op_checksigverify(&mut stack);
        assert!(!b);
        // wrong public key
        // op_checksig([msg,sig,pk']) -> fail
        let (pk, _sk) = sign::gen_keypair();
        let msg = hex::encode(vec![0, 0, 0]);
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes(msg));
        stack.push(StackEntry::Signature(sig));
        stack.push(StackEntry::PubKey(pk));
        let b = op_checksigverify(&mut stack);
        assert!(!b);
        // no message
        // op_checksigverify([sig,pk]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Signature(sig));
        stack.push(StackEntry::PubKey(pk));
        let b = op_checksigverify(&mut stack);
        assert!(!b)
    }

    #[test]
    // Test OP_CHECKMULTISIG
    fn test_checkmultisig() {
        // 2-of-3 multisig
        // op_checkmultisig([msg,sig1,sig2,2,pk1,pk2,pk3,3]) -> [1]
        let (pk1, sk1) = sign::gen_keypair();
        let (pk2, sk2) = sign::gen_keypair();
        let (pk3, sk3) = sign::gen_keypair();
        let msg = hex::encode(vec![0, 0, 0]);
        let sig1 = sign::sign_detached(msg.as_bytes(), &sk1);
        let sig2 = sign::sign_detached(msg.as_bytes(), &sk2);
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes(msg));
        stack.push(StackEntry::Signature(sig1));
        stack.push(StackEntry::Signature(sig2));
        stack.push(StackEntry::Num(2));
        stack.push(StackEntry::PubKey(pk1));
        stack.push(StackEntry::PubKey(pk2));
        stack.push(StackEntry::PubKey(pk3));
        stack.push(StackEntry::Num(3));
        let v: Vec<StackEntry> = vec![StackEntry::Num(1)];
        op_checkmultisig(&mut stack);
        assert_eq!(stack.main_stack, v);
        // 0-of-3 multisig
        // op_checkmultisig([msg,0,pk1,pk2,pk3,3]) -> [1]
        let msg = hex::encode(vec![0, 0, 0]);
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes(msg));
        stack.push(StackEntry::Num(0));
        stack.push(StackEntry::PubKey(pk1));
        stack.push(StackEntry::PubKey(pk2));
        stack.push(StackEntry::PubKey(pk3));
        stack.push(StackEntry::Num(3));
        let v: Vec<StackEntry> = vec![StackEntry::Num(1)];
        op_checkmultisig(&mut stack);
        assert_eq!(stack.main_stack, v);
        // 0-of-0 multisig
        // op_checkmultisig([msg,0,0]) -> [1]
        let msg = hex::encode(vec![0, 0, 0]);
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes(msg));
        stack.push(StackEntry::Num(0));
        stack.push(StackEntry::Num(0));
        let v: Vec<StackEntry> = vec![StackEntry::Num(1)];
        op_checkmultisig(&mut stack);
        assert_eq!(stack.main_stack, v);
        // 1-of-1 multisig
        // op_checkmultisig([msg,sig1,1,pk1,1]) -> [1]
        let msg = hex::encode(vec![0, 0, 0]);
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes(msg));
        stack.push(StackEntry::Signature(sig1));
        stack.push(StackEntry::Num(1));
        stack.push(StackEntry::PubKey(pk1));
        stack.push(StackEntry::Num(1));
        let v: Vec<StackEntry> = vec![StackEntry::Num(1)];
        op_checkmultisig(&mut stack);
        assert_eq!(stack.main_stack, v);
        // ordering is not relevant
        // op_checkmultisig([msg,sig3,sig1,2,pk2,pk3,pk1,3]) -> [1]
        let msg = hex::encode(vec![0, 0, 0]);
        let sig3 = sign::sign_detached(msg.as_bytes(), &sk3);
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes(msg));
        stack.push(StackEntry::Signature(sig3));
        stack.push(StackEntry::Signature(sig1));
        stack.push(StackEntry::Num(2));
        stack.push(StackEntry::PubKey(pk2));
        stack.push(StackEntry::PubKey(pk3));
        stack.push(StackEntry::PubKey(pk1));
        stack.push(StackEntry::Num(3));
        let v: Vec<StackEntry> = vec![StackEntry::Num(1)];
        op_checkmultisig(&mut stack);
        assert_eq!(stack.main_stack, v);
        // wrong message
        // op_checkmultisig([msg',sig1,sig2,2,pk1,pk2,pk3,3]) -> [0]
        let msg = hex::encode(vec![0, 0, 1]);
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes(msg));
        stack.push(StackEntry::Signature(sig1));
        stack.push(StackEntry::Signature(sig2));
        stack.push(StackEntry::Num(2));
        stack.push(StackEntry::PubKey(pk1));
        stack.push(StackEntry::PubKey(pk2));
        stack.push(StackEntry::PubKey(pk3));
        stack.push(StackEntry::Num(3));
        let v: Vec<StackEntry> = vec![StackEntry::Num(0)];
        op_checkmultisig(&mut stack);
        assert_eq!(stack.main_stack, v);
        // same signature twice
        // op_checkmultisig([msg,sig1,sig1,2,pk1,pk2,pk3,3]) -> [0]
        let msg = hex::encode(vec![0, 0, 0]);
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes(msg));
        stack.push(StackEntry::Signature(sig1));
        stack.push(StackEntry::Signature(sig1));
        stack.push(StackEntry::Num(2));
        stack.push(StackEntry::PubKey(pk1));
        stack.push(StackEntry::PubKey(pk2));
        stack.push(StackEntry::PubKey(pk3));
        stack.push(StackEntry::Num(3));
        let v: Vec<StackEntry> = vec![StackEntry::Num(0)];
        op_checkmultisig(&mut stack);
        assert_eq!(stack.main_stack, v);
        // too many pubkeys
        // op_checkmultisig([MAX_PUB_KEYS_PER_MULTISIG+1]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(MAX_PUB_KEYS_PER_MULTISIG as usize + 1));
        let b = op_checkmultisig(&mut stack);
        assert!(!b);
        // not enough pubkeys
        // op_checkmultisig([pk1,pk2,3]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::PubKey(pk1));
        stack.push(StackEntry::PubKey(pk2));
        stack.push(StackEntry::Num(3));
        let b = op_checkmultisig(&mut stack);
        assert!(!b);
        // too many signatures
        // op_checkmultisig([4,pk1,pk2,pk3,3]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(4));
        stack.push(StackEntry::PubKey(pk1));
        stack.push(StackEntry::PubKey(pk2));
        stack.push(StackEntry::PubKey(pk3));
        stack.push(StackEntry::Num(3));
        let b = op_checkmultisig(&mut stack);
        assert!(!b);
        // not enough signatures
        // op_checkmultisig([sig1,2,pk1,pk2,pk3,3]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Signature(sig1));
        stack.push(StackEntry::Num(2));
        stack.push(StackEntry::PubKey(pk1));
        stack.push(StackEntry::PubKey(pk2));
        stack.push(StackEntry::PubKey(pk3));
        stack.push(StackEntry::Num(3));
        let b = op_checkmultisig(&mut stack);
        assert!(!b);
        // no message
        // op_checkmultisig([sig1,sig2,2,pk1,pk2,pk3,3]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Signature(sig1));
        stack.push(StackEntry::Signature(sig2));
        stack.push(StackEntry::Num(2));
        stack.push(StackEntry::PubKey(pk1));
        stack.push(StackEntry::PubKey(pk2));
        stack.push(StackEntry::PubKey(pk3));
        stack.push(StackEntry::Num(3));
        let b = op_checkmultisig(&mut stack);
        assert!(!b);
    }

    #[test]
    // Test OP_CHECKMULTISIGVERIFY
    fn test_checkmultisigverify() {
        // 2-of-3 multisig
        // op_checkmultisigverify([msg,sig1,sig2,2,pk1,pk2,pk3,3]) -> []
        let (pk1, sk1) = sign::gen_keypair();
        let (pk2, sk2) = sign::gen_keypair();
        let (pk3, sk3) = sign::gen_keypair();
        let msg = hex::encode(vec![0, 0, 0]);
        let sig1 = sign::sign_detached(msg.as_bytes(), &sk1);
        let sig2 = sign::sign_detached(msg.as_bytes(), &sk2);
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes(msg));
        stack.push(StackEntry::Signature(sig1));
        stack.push(StackEntry::Signature(sig2));
        stack.push(StackEntry::Num(2));
        stack.push(StackEntry::PubKey(pk1));
        stack.push(StackEntry::PubKey(pk2));
        stack.push(StackEntry::PubKey(pk3));
        stack.push(StackEntry::Num(3));
        let v: Vec<StackEntry> = vec![];
        op_checkmultisigverify(&mut stack);
        assert_eq!(stack.main_stack, v);
        // 0-of-3 multisig
        // op_checkmultisigverify([msg,0,pk1,pk2,pk3,3]) -> []
        let msg = hex::encode(vec![0, 0, 0]);
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes(msg));
        stack.push(StackEntry::Num(0));
        stack.push(StackEntry::PubKey(pk1));
        stack.push(StackEntry::PubKey(pk2));
        stack.push(StackEntry::PubKey(pk3));
        stack.push(StackEntry::Num(3));
        let v: Vec<StackEntry> = vec![];
        op_checkmultisigverify(&mut stack);
        assert_eq!(stack.main_stack, v);
        // 0-of-0 multisig
        // op_checkmultisig([msg,0,0]) -> []
        let msg = hex::encode(vec![0, 0, 0]);
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes(msg));
        stack.push(StackEntry::Num(0));
        stack.push(StackEntry::Num(0));
        let v: Vec<StackEntry> = vec![];
        op_checkmultisigverify(&mut stack);
        assert_eq!(stack.main_stack, v);
        // 1-of-1 multisig
        // op_checkmultisigverify([msg,sig1,1,pk1,1]) -> []
        let msg = hex::encode(vec![0, 0, 0]);
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes(msg));
        stack.push(StackEntry::Signature(sig1));
        stack.push(StackEntry::Num(1));
        stack.push(StackEntry::PubKey(pk1));
        stack.push(StackEntry::Num(1));
        let v: Vec<StackEntry> = vec![];
        op_checkmultisigverify(&mut stack);
        assert_eq!(stack.main_stack, v);
        // ordering is not relevant
        // op_checkmultisigverify([msg,sig3,sig1,2,pk2,pk3,pk1,3]) -> []
        let msg = hex::encode(vec![0, 0, 0]);
        let sig3 = sign::sign_detached(msg.as_bytes(), &sk3);
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes(msg));
        stack.push(StackEntry::Signature(sig3));
        stack.push(StackEntry::Signature(sig1));
        stack.push(StackEntry::Num(2));
        stack.push(StackEntry::PubKey(pk2));
        stack.push(StackEntry::PubKey(pk3));
        stack.push(StackEntry::PubKey(pk1));
        stack.push(StackEntry::Num(3));
        let v: Vec<StackEntry> = vec![];
        op_checkmultisigverify(&mut stack);
        assert_eq!(stack.main_stack, v);
        // wrong message
        // op_checkmultisigverify([msg',sig1,sig2,2,pk1,pk2,pk3,3]) -> fail
        let msg = hex::encode(vec![0, 0, 1]);
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes(msg));
        stack.push(StackEntry::Signature(sig1));
        stack.push(StackEntry::Signature(sig2));
        stack.push(StackEntry::Num(2));
        stack.push(StackEntry::PubKey(pk1));
        stack.push(StackEntry::PubKey(pk2));
        stack.push(StackEntry::PubKey(pk3));
        stack.push(StackEntry::Num(3));
        let b = op_checkmultisigverify(&mut stack);
        assert!(!b);
        // same signature twice
        // op_checkmultisigverify([msg,sig1,sig1,2,pk1,pk2,pk3,3]) -> fail
        let msg = hex::encode(vec![0, 0, 0]);
        let mut stack = Stack::new();
        stack.push(StackEntry::Bytes(msg));
        stack.push(StackEntry::Signature(sig1));
        stack.push(StackEntry::Signature(sig1));
        stack.push(StackEntry::Num(2));
        stack.push(StackEntry::PubKey(pk1));
        stack.push(StackEntry::PubKey(pk2));
        stack.push(StackEntry::PubKey(pk3));
        stack.push(StackEntry::Num(3));
        op_checkmultisigverify(&mut stack);
        assert_eq!(stack.main_stack, v);
        // too many pubkeys
        // op_checkmultisigverify([MAX_PUB_KEYS_PER_MULTISIG+1]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(MAX_PUB_KEYS_PER_MULTISIG as usize + 1));
        let b = op_checkmultisigverify(&mut stack);
        assert!(!b);
        // not enough pubkeys
        // op_checkmultisigverify([pk1,pk2,3]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::PubKey(pk1));
        stack.push(StackEntry::PubKey(pk2));
        stack.push(StackEntry::Num(3));
        let b = op_checkmultisigverify(&mut stack);
        assert!(!b);
        // too many signatures
        // op_checkmultisigverify([4,pk1,pk2,pk3,3]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Num(4));
        stack.push(StackEntry::PubKey(pk1));
        stack.push(StackEntry::PubKey(pk2));
        stack.push(StackEntry::PubKey(pk3));
        stack.push(StackEntry::Num(3));
        let b = op_checkmultisigverify(&mut stack);
        assert!(!b);
        // not enough signatures
        // op_checkmultisigverify([sig1,2,pk1,pk2,pk3,3]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Signature(sig1));
        stack.push(StackEntry::Num(2));
        stack.push(StackEntry::PubKey(pk1));
        stack.push(StackEntry::PubKey(pk2));
        stack.push(StackEntry::PubKey(pk3));
        stack.push(StackEntry::Num(3));
        let b = op_checkmultisigverify(&mut stack);
        assert!(!b);
        // no message
        // op_checkmultisigverify([sig1,sig2,2,pk1,pk2,pk3,3]) -> fail
        let mut stack = Stack::new();
        stack.push(StackEntry::Signature(sig1));
        stack.push(StackEntry::Signature(sig2));
        stack.push(StackEntry::Num(2));
        stack.push(StackEntry::PubKey(pk1));
        stack.push(StackEntry::PubKey(pk2));
        stack.push(StackEntry::PubKey(pk3));
        stack.push(StackEntry::Num(3));
        let b = op_checkmultisigverify(&mut stack);
        assert!(!b);
    }

    #[test]
    fn test_is_valid_script() {
        // empty script
        let v = vec![];
        let script = Script::from(v);
        assert!(script.is_valid());
        // script length <= 10000 bytes
        let v = vec![StackEntry::Bytes("a".repeat(500)); 20];
        let script = Script::from(v);
        assert!(script.is_valid());
        // script length > 10000 bytes
        let v = vec![StackEntry::Bytes("a".repeat(500)); 21];
        let script = Script::from(v);
        assert!(!script.is_valid());
        // # opcodes <= 201
        let v = vec![StackEntry::Op(OpCodes::OP_1); MAX_OPS_PER_SCRIPT as usize];
        let script = Script::from(v);
        assert!(script.is_valid());
        // # opcodes > 201
        let v = vec![StackEntry::Op(OpCodes::OP_1); (MAX_OPS_PER_SCRIPT + 1) as usize];
        let script = Script::from(v);
        assert!(!script.is_valid());
    }

    #[test]
    fn test_is_valid_stack() {
        // empty stack
        let v = vec![];
        let stack = Stack::from(v);
        assert!(stack.is_valid());
        // # items on interpreter stack <= 1000
        let v = vec![StackEntry::Num(1); MAX_STACK_SIZE as usize];
        let stack = Stack::from(v);
        assert!(stack.is_valid());
        // # items on interpreter stack > 1000
        let v = vec![StackEntry::Num(1); (MAX_STACK_SIZE + 1) as usize];
        let stack = Stack::from(v);
        assert!(!stack.is_valid());
    }

    #[test]
    fn test_interpret_script() {
        // empty script
        let v = vec![];
        let script = Script::from(v);
        assert!(script.interpret());
        // OP_0
        let v = vec![StackEntry::Op(OpCodes::OP_0)];
        let script = Script::from(v);
        assert!(!script.interpret());
        // OP_1
        let v = vec![StackEntry::Op(OpCodes::OP_1)];
        let script = Script::from(v);
        assert!(script.interpret());
        // OP_1 OP_2 OP_ADD OP_3 OP_EQUAL
        let v = vec![
            StackEntry::Op(OpCodes::OP_1),
            StackEntry::Op(OpCodes::OP_2),
            StackEntry::Op(OpCodes::OP_ADD),
            StackEntry::Op(OpCodes::OP_3),
            StackEntry::Op(OpCodes::OP_EQUAL),
        ];
        let script = Script::from(v);
        assert!(script.interpret());
        // script length <= 10000 bytes
        let v = vec![StackEntry::Bytes("a".repeat(500)); 20];
        let script = Script::from(v);
        assert!(script.interpret());
        // script length > 10000 bytes
        let v = vec![StackEntry::Bytes("a".repeat(500)); 21];
        let script = Script::from(v);
        assert!(!script.interpret());
        // # opcodes <= 201
        let v = vec![StackEntry::Op(OpCodes::OP_1); MAX_OPS_PER_SCRIPT as usize];
        let script = Script::from(v);
        assert!(script.interpret());
        // # opcodes > 201
        let v = vec![StackEntry::Op(OpCodes::OP_1); (MAX_OPS_PER_SCRIPT + 1) as usize];
        let script = Script::from(v);
        assert!(!script.interpret());
        // # items on interpreter stack <= 1000
        let v = vec![StackEntry::Num(1); MAX_STACK_SIZE as usize];
        let script = Script::from(v);
        assert!(script.interpret());
        // # items on interpreter stack > 1000
        let v = vec![StackEntry::Num(1); (MAX_STACK_SIZE + 1) as usize];
        let script = Script::from(v);
        assert!(!script.interpret());
    }

    #[test]
    fn test_conditionals() {
        // OP_1 OP_IF OP_2 OP_ELSE OP_3 OP_ELSE OP_0 OP_ENDIF
        let v = vec![
            StackEntry::Op(OpCodes::OP_1),
            StackEntry::Op(OpCodes::OP_IF),
            StackEntry::Op(OpCodes::OP_2),
            StackEntry::Op(OpCodes::OP_ELSE),
            StackEntry::Op(OpCodes::OP_3),
            StackEntry::Op(OpCodes::OP_ELSE),
            StackEntry::Op(OpCodes::OP_0),
            StackEntry::Op(OpCodes::OP_ENDIF),
        ];
        let script = Script::from(v);
        assert!(!script.interpret());
        // OP_1 OP_IF OP_2 OP_ELSE OP_3 OP_ENDIF
        let v = vec![
            StackEntry::Op(OpCodes::OP_1),
            StackEntry::Op(OpCodes::OP_IF),
            StackEntry::Op(OpCodes::OP_2),
            StackEntry::Op(OpCodes::OP_ELSE),
            StackEntry::Op(OpCodes::OP_3),
            StackEntry::Op(OpCodes::OP_ENDIF),
        ];
        let script = Script::from(v);
        assert!(script.interpret());
        // OP_1 OP_IF OP_0 OP_ELSE OP_3 OP_ENDIF
        let v = vec![
            StackEntry::Op(OpCodes::OP_1),
            StackEntry::Op(OpCodes::OP_IF),
            StackEntry::Op(OpCodes::OP_0),
            StackEntry::Op(OpCodes::OP_ELSE),
            StackEntry::Op(OpCodes::OP_3),
            StackEntry::Op(OpCodes::OP_ENDIF),
        ];
        let script = Script::from(v);
        assert!(!script.interpret());
        // OP_0 OP_IF OP_2 OP_ELSE OP_3 OP_ENDIF
        let v = vec![
            StackEntry::Op(OpCodes::OP_0),
            StackEntry::Op(OpCodes::OP_IF),
            StackEntry::Op(OpCodes::OP_2),
            StackEntry::Op(OpCodes::OP_ELSE),
            StackEntry::Op(OpCodes::OP_3),
            StackEntry::Op(OpCodes::OP_ENDIF),
        ];
        let script = Script::from(v);
        assert!(script.interpret());
        // OP_0 OP_IF OP_2 OP_ELSE OP_0 OP_ENDIF
        let v = vec![
            StackEntry::Op(OpCodes::OP_0),
            StackEntry::Op(OpCodes::OP_IF),
            StackEntry::Op(OpCodes::OP_2),
            StackEntry::Op(OpCodes::OP_ELSE),
            StackEntry::Op(OpCodes::OP_0),
            StackEntry::Op(OpCodes::OP_ENDIF),
        ];
        let script = Script::from(v);
        assert!(!script.interpret());
        // OP_0 OP_NOTIF OP_2 OP_ELSE OP_0 OP_ENDIF
        let v = vec![
            StackEntry::Op(OpCodes::OP_0),
            StackEntry::Op(OpCodes::OP_NOTIF),
            StackEntry::Op(OpCodes::OP_2),
            StackEntry::Op(OpCodes::OP_ELSE),
            StackEntry::Op(OpCodes::OP_0),
            StackEntry::Op(OpCodes::OP_ENDIF),
        ];
        let script = Script::from(v);
        assert!(script.interpret());
        // OP_0 OP_IF OP_2 OP_ENDIF
        let v = vec![
            StackEntry::Op(OpCodes::OP_1),
            StackEntry::Op(OpCodes::OP_IF),
            StackEntry::Op(OpCodes::OP_0),
            StackEntry::Op(OpCodes::OP_ENDIF),
        ];
        let script = Script::from(v);
        assert!(!script.interpret());
        // OP_1 OP_IF OP_2 OP_IF OP_3 OP_ELSE OP_0 OP_ENDIF OP_ENDIF
        let v = vec![
            StackEntry::Op(OpCodes::OP_1),
            StackEntry::Op(OpCodes::OP_IF),
            StackEntry::Op(OpCodes::OP_2),
            StackEntry::Op(OpCodes::OP_IF),
            StackEntry::Op(OpCodes::OP_3),
            StackEntry::Op(OpCodes::OP_ELSE),
            StackEntry::Op(OpCodes::OP_0),
            StackEntry::Op(OpCodes::OP_ENDIF),
            StackEntry::Op(OpCodes::OP_ENDIF),
        ];
        let script = Script::from(v);
        assert!(script.interpret());
        // OP_1 OP_IF OP_0 OP_IF OP_3 OP_ELSE OP_0 OP_ENDIF OP_ENDIF
        let v = vec![
            StackEntry::Op(OpCodes::OP_1),
            StackEntry::Op(OpCodes::OP_IF),
            StackEntry::Op(OpCodes::OP_0),
            StackEntry::Op(OpCodes::OP_IF),
            StackEntry::Op(OpCodes::OP_3),
            StackEntry::Op(OpCodes::OP_ELSE),
            StackEntry::Op(OpCodes::OP_0),
            StackEntry::Op(OpCodes::OP_ENDIF),
            StackEntry::Op(OpCodes::OP_ENDIF),
        ];
        let script = Script::from(v);
        assert!(!script.interpret());
        // OP_0 OP_IF OP_2 OP_IF OP_3 OP_ELSE OP_4 OP_ENDIF OP_ELSE OP_0 OP_ENDIF
        let v = vec![
            StackEntry::Op(OpCodes::OP_0),
            StackEntry::Op(OpCodes::OP_IF),
            StackEntry::Op(OpCodes::OP_2),
            StackEntry::Op(OpCodes::OP_IF),
            StackEntry::Op(OpCodes::OP_3),
            StackEntry::Op(OpCodes::OP_ELSE),
            StackEntry::Op(OpCodes::OP_4),
            StackEntry::Op(OpCodes::OP_ENDIF),
            StackEntry::Op(OpCodes::OP_ELSE),
            StackEntry::Op(OpCodes::OP_0),
            StackEntry::Op(OpCodes::OP_ENDIF),
        ];
        let script = Script::from(v);
        assert!(!script.interpret());
        // OP_1 OP_IF OP_1
        let v = vec![
            StackEntry::Op(OpCodes::OP_1),
            StackEntry::Op(OpCodes::OP_IF),
            StackEntry::Op(OpCodes::OP_1),
        ];
        let script = Script::from(v);
        assert!(!script.interpret());
        // OP_1 OP_IF OP_1 OP_ELSE OP_3
        let v = vec![
            StackEntry::Op(OpCodes::OP_1),
            StackEntry::Op(OpCodes::OP_IF),
            StackEntry::Op(OpCodes::OP_1),
            StackEntry::Op(OpCodes::OP_ELSE),
            StackEntry::Op(OpCodes::OP_3),
        ];
        let script = Script::from(v);
        assert!(!script.interpret());
        // OP_2 OP_ELSE OP_3 OP_ENDIF
        let v = vec![
            StackEntry::Op(OpCodes::OP_2),
            StackEntry::Op(OpCodes::OP_ELSE),
            StackEntry::Op(OpCodes::OP_3),
            StackEntry::Op(OpCodes::OP_ENDIF),
        ];
        let script = Script::from(v);
        assert!(!script.interpret());
        // OP_IF
        let v = vec![StackEntry::Op(OpCodes::OP_IF)];
        let script = Script::from(v);
        assert!(!script.interpret());
        // OP_NOTIF
        let v = vec![StackEntry::Op(OpCodes::OP_NOTIF)];
        let script = Script::from(v);
        assert!(!script.interpret());
        // OP_ELSE
        let v = vec![StackEntry::Op(OpCodes::OP_ELSE)];
        let script = Script::from(v);
        assert!(!script.interpret());
        // OP_ENDIF
        let v = vec![StackEntry::Op(OpCodes::OP_ENDIF)];
        let script = Script::from(v);
        assert!(!script.interpret());
    }

    #[test]
    fn test_burn_script() {
        let v = vec![StackEntry::Op(OpCodes::OP_BURN)];
        let script = Script::from(v);
        assert!(!script.interpret());
    }
}
