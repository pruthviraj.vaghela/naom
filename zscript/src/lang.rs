use crate::constants::*;
use crate::error_utils::*;
use crate::interface_ops::*;
use crate::{OpCodes, StackEntry};
use serde::{Deserialize, Serialize};
use zcrypto::sign_ed25519::{PublicKey, Signature, ED25519_PUBLIC_KEY_LEN, ED25519_SIGNATURE_LEN};
use zcrypto::utils::construct_address_for;

/// Stack for script execution
#[derive(Clone, Debug, PartialOrd, Eq, PartialEq, Serialize, Deserialize)]
pub struct Stack {
    pub main_stack: Vec<StackEntry>,
    pub alt_stack: Vec<StackEntry>,
}

impl Default for Stack {
    fn default() -> Self {
        Self::new()
    }
}

impl Stack {
    /// Creates a new stack
    pub fn new() -> Self {
        Self {
            main_stack: Vec::with_capacity(MAX_STACK_SIZE as usize),
            alt_stack: Vec::with_capacity(MAX_STACK_SIZE as usize),
        }
    }

    /// Checks if the stack is valid
    pub fn is_valid(&self) -> bool {
        if self.main_stack.len() + self.alt_stack.len() > MAX_STACK_SIZE as usize {
            error_max_stack_size();
            return false;
        }
        true
    }

    /// Pops the top item from the stack
    pub fn pop(&mut self) -> Option<StackEntry> {
        self.main_stack.pop()
    }

    /// Returns the top item on the stack
    pub fn last(&self) -> Option<StackEntry> {
        self.main_stack.last().cloned()
    }

    /// Checks if the last item on the stack is not zero
    pub fn is_last_non_zero(&self) -> bool {
        self.last() != Some(StackEntry::Num(0))
    }

    /// Pushes a new entry onto the stack
    pub fn push(&mut self, stack_entry: StackEntry) -> bool {
        match stack_entry.clone() {
            StackEntry::Op(_) => {
                return false;
            }
            StackEntry::Bytes(s) => {
                if s.len() > MAX_SCRIPT_ITEM_SIZE as usize {
                    return false;
                }
            }
            _ => (),
        }
        self.main_stack.push(stack_entry);
        true
    }
}

impl From<Vec<StackEntry>> for Stack {
    /// Creates a new stack with a pre-filled main stack
    fn from(stack: Vec<StackEntry>) -> Self {
        Stack {
            main_stack: stack,
            alt_stack: Vec::with_capacity(MAX_STACK_SIZE as usize),
        }
    }
}

/// Stack for conditionals
#[derive(Clone, Debug, PartialOrd, Eq, PartialEq, Serialize, Deserialize)]
pub struct ConditionStack {
    pub size: usize,
    pub first_false_pos: Option<usize>,
}

impl Default for ConditionStack {
    fn default() -> Self {
        Self::new()
    }
}

impl ConditionStack {
    /// Creates a new stack for conditionals
    pub fn new() -> Self {
        Self {
            size: 0,
            first_false_pos: None,
        }
    }

    /// Checks if all values are true
    pub fn all_true(&self) -> bool {
        self.first_false_pos.is_none()
    }

    /// Checks if the condition stack is empty
    pub fn is_empty(&self) -> bool {
        self.size == 0
    }

    /// Pushes a new value onto the condition stack
    pub fn push(&mut self, cond: bool) {
        if self.first_false_pos.is_none() && !cond {
            self.first_false_pos = Some(self.size);
        }
        self.size += 1;
    }

    /// Pops the top value from the condition stack
    pub fn pop(&mut self) {
        self.size -= 1;
        if let Some(pos) = self.first_false_pos {
            if pos == self.size {
                self.first_false_pos.take();
            }
        }
    }

    /// Toggles the top value on the condition stack
    pub fn toggle(&mut self) {
        match self.first_false_pos {
            Some(pos) => {
                if pos == self.size - 1 {
                    self.first_false_pos = None;
                }
            }
            _ => {
                self.first_false_pos = Some(self.size - 1);
            }
        }
    }
}

/// Scripts are defined as a sequence of stack entries
/// NOTE: A tuple struct could probably work here as well
#[derive(Clone, Debug, PartialOrd, Eq, PartialEq, Serialize, Deserialize)]
pub struct Script {
    pub stack: Vec<StackEntry>,
}

impl Default for Script {
    fn default() -> Self {
        Self::new()
    }
}

impl Script {
    /// Constructs a new script
    pub fn new() -> Self {
        Self { stack: Vec::new() }
    }

    /// Checks if a script is valid
    pub fn is_valid(&self) -> bool {
        let mut len = 0; // script length in bytes
        let mut ops_count = 0; // number of opcodes in script
        for entry in &self.stack {
            match entry {
                StackEntry::Op(_) => {
                    len += 1;
                    ops_count += 1;
                }
                StackEntry::Signature(_) => len += ED25519_SIGNATURE_LEN,
                StackEntry::PubKey(_) => len += ED25519_PUBLIC_KEY_LEN,
                StackEntry::Bytes(s) => len += s.len(),
                StackEntry::Num(_) => len += usize::BITS as usize / 8,
            };
        }
        if len > MAX_SCRIPT_SIZE as usize {
            error_max_script_size();
            return false;
        }
        if ops_count > MAX_OPS_PER_SCRIPT as usize {
            error_max_ops_script();
            return false;
        }
        true
    }

    fn interpret_opcode(op: OpCodes, stack: &mut Stack, cond_stack: &mut ConditionStack) -> bool {
        match op {
            // constants
            OpCodes::OP_0 => stack.push(StackEntry::Num(0)),
            OpCodes::OP_1 => stack.push(StackEntry::Num(1)),
            OpCodes::OP_2 => stack.push(StackEntry::Num(2)),
            OpCodes::OP_3 => stack.push(StackEntry::Num(3)),
            OpCodes::OP_4 => stack.push(StackEntry::Num(4)),
            OpCodes::OP_5 => stack.push(StackEntry::Num(5)),
            OpCodes::OP_6 => stack.push(StackEntry::Num(6)),
            OpCodes::OP_7 => stack.push(StackEntry::Num(7)),
            OpCodes::OP_8 => stack.push(StackEntry::Num(8)),
            OpCodes::OP_9 => stack.push(StackEntry::Num(9)),
            OpCodes::OP_10 => stack.push(StackEntry::Num(10)),
            OpCodes::OP_11 => stack.push(StackEntry::Num(11)),
            OpCodes::OP_12 => stack.push(StackEntry::Num(12)),
            OpCodes::OP_13 => stack.push(StackEntry::Num(13)),
            OpCodes::OP_14 => stack.push(StackEntry::Num(14)),
            OpCodes::OP_15 => stack.push(StackEntry::Num(15)),
            OpCodes::OP_16 => stack.push(StackEntry::Num(16)),
            // flow control
            OpCodes::OP_NOP => op_nop(stack),
            OpCodes::OP_IF => op_if(stack, cond_stack),
            OpCodes::OP_NOTIF => op_notif(stack, cond_stack),
            OpCodes::OP_ELSE => op_else(cond_stack),
            OpCodes::OP_ENDIF => op_endif(cond_stack),
            OpCodes::OP_VERIFY => op_verify(stack),
            OpCodes::OP_BURN => op_burn(stack),
            // stack
            OpCodes::OP_TOALTSTACK => op_toaltstack(stack),
            OpCodes::OP_FROMALTSTACK => op_fromaltstack(stack),
            OpCodes::OP_2DROP => op_2drop(stack),
            OpCodes::OP_2DUP => op_2dup(stack),
            OpCodes::OP_3DUP => op_3dup(stack),
            OpCodes::OP_2OVER => op_2over(stack),
            OpCodes::OP_2ROT => op_2rot(stack),
            OpCodes::OP_2SWAP => op_2swap(stack),
            OpCodes::OP_IFDUP => op_ifdup(stack),
            OpCodes::OP_DEPTH => op_depth(stack),
            OpCodes::OP_DROP => op_drop(stack),
            OpCodes::OP_DUP => op_dup(stack),
            OpCodes::OP_NIP => op_nip(stack),
            OpCodes::OP_OVER => op_over(stack),
            OpCodes::OP_PICK => op_pick(stack),
            OpCodes::OP_ROLL => op_roll(stack),
            OpCodes::OP_ROT => op_rot(stack),
            OpCodes::OP_SWAP => op_swap(stack),
            OpCodes::OP_TUCK => op_tuck(stack),
            // splice
            OpCodes::OP_CAT => op_cat(stack),
            OpCodes::OP_SUBSTR => op_substr(stack),
            OpCodes::OP_LEFT => op_left(stack),
            OpCodes::OP_RIGHT => op_right(stack),
            OpCodes::OP_SIZE => op_size(stack),
            // bitwise logic
            OpCodes::OP_INVERT => op_invert(stack),
            OpCodes::OP_AND => op_and(stack),
            OpCodes::OP_OR => op_or(stack),
            OpCodes::OP_XOR => op_xor(stack),
            OpCodes::OP_EQUAL => op_equal(stack),
            OpCodes::OP_EQUALVERIFY => op_equalverify(stack),
            // arithmetic
            OpCodes::OP_1ADD => op_1add(stack),
            OpCodes::OP_1SUB => op_1sub(stack),
            OpCodes::OP_2MUL => op_2mul(stack),
            OpCodes::OP_2DIV => op_2div(stack),
            OpCodes::OP_NOT => op_not(stack),
            OpCodes::OP_0NOTEQUAL => op_0notequal(stack),
            OpCodes::OP_ADD => op_add(stack),
            OpCodes::OP_SUB => op_sub(stack),
            OpCodes::OP_MUL => op_mul(stack),
            OpCodes::OP_DIV => op_div(stack),
            OpCodes::OP_MOD => op_mod(stack),
            OpCodes::OP_LSHIFT => op_lshift(stack),
            OpCodes::OP_RSHIFT => op_rshift(stack),
            OpCodes::OP_BOOLAND => op_booland(stack),
            OpCodes::OP_BOOLOR => op_boolor(stack),
            OpCodes::OP_NUMEQUAL => op_numequal(stack),
            OpCodes::OP_NUMEQUALVERIFY => op_numequalverify(stack),
            OpCodes::OP_NUMNOTEQUAL => op_numnotequal(stack),
            OpCodes::OP_LESSTHAN => op_lessthan(stack),
            OpCodes::OP_GREATERTHAN => op_greaterthan(stack),
            OpCodes::OP_LESSTHANOREQUAL => op_lessthanorequal(stack),
            OpCodes::OP_GREATERTHANOREQUAL => op_greaterthanorequal(stack),
            OpCodes::OP_MIN => op_min(stack),
            OpCodes::OP_MAX => op_max(stack),
            OpCodes::OP_WITHIN => op_within(stack),
            // crypto
            OpCodes::OP_SHA3 => op_sha3(stack),
            OpCodes::OP_HASH256 => op_hash256(stack),
            OpCodes::OP_HASH256_V0 => op_hash256_v0(stack),
            OpCodes::OP_HASH256_TEMP => op_hash256_temp(stack),
            OpCodes::OP_CHECKSIG => op_checksig(stack),
            OpCodes::OP_CHECKSIGVERIFY => op_checksigverify(stack),
            OpCodes::OP_CHECKMULTISIG => op_checkmultisig(stack),
            OpCodes::OP_CHECKMULTISIGVERIFY => op_checkmultisigverify(stack),
            // smart data
            OpCodes::OP_CREATE => true,
            // reserved
            _ => true,
        }
    }

    /// Interprets and executes a single stack entry.
    ///
    /// Returns `Option<bool>`: if it is `None`, then the stack entry was skipped.
    /// If it is `Some(bool)`, then the `bool` will contain the execution result.
    pub fn interpret_stack_entry(
        stack_entry: &StackEntry,
        stack: &mut Stack,
        cond_stack: &mut ConditionStack,
    ) -> Option<bool> {
        match stack_entry {
            StackEntry::Op(op) => Some(Self::interpret_opcode(op.clone(), stack, cond_stack)),
            StackEntry::Signature(_)
            | StackEntry::PubKey(_)
            | StackEntry::Num(_)
            | StackEntry::Bytes(_) => {
                if cond_stack.all_true() {
                    Some(stack.push(stack_entry.clone()))
                } else {
                    None
                }
            }
        }
    }

    /// Interprets and executes a script
    pub fn interpret(&self) -> bool {
        if !self.is_valid() {
            return false;
        }
        let mut stack = Stack::new();
        let mut cond_stack = ConditionStack::new();
        let mut test_for_return = true;
        for stack_entry in &self.stack {
            if let StackEntry::Op(op) = &stack_entry {
                if !cond_stack.all_true() && !op.is_conditional() {
                    // skip opcode if latest condition check failed
                    continue;
                }
            }
            match Self::interpret_stack_entry(stack_entry, &mut stack, &mut cond_stack) {
                Some(val) => {
                    test_for_return &= val;
                }
                None => continue,
            }
            if !test_for_return || !stack.is_valid() {
                return false;
            }
        }
        test_for_return && stack.is_last_non_zero() && cond_stack.is_empty()
    }

    /// Constructs a new script for coinbase
    ///
    /// ### Arguments
    ///
    /// * `block_number`  - The block time to push
    pub fn new_for_coinbase(block_number: u64) -> Self {
        let stack = vec![StackEntry::Num(block_number as usize)];
        Self { stack }
    }

    /// Constructs a new script for an asset creation
    ///
    /// ### Arguments
    ///
    /// * `block_number`    - The block time
    /// * `asset_hash`      - The hash of the asset
    /// * `signature`       - The signature of the asset contents
    /// * `pub_key`         - The public key used in creating the signed content
    pub fn new_create_asset(
        block_number: u64,
        asset_hash: String,
        signature: Signature,
        pub_key: PublicKey,
    ) -> Self {
        let stack = vec![
            StackEntry::Op(OpCodes::OP_CREATE),
            StackEntry::Num(block_number as usize),
            StackEntry::Op(OpCodes::OP_DROP),
            StackEntry::Bytes(asset_hash),
            StackEntry::Signature(signature),
            StackEntry::PubKey(pub_key),
            StackEntry::Op(OpCodes::OP_CHECKSIG),
        ];
        Self { stack }
    }

    /// Constructs a pay to public key hash script
    ///
    /// ### Arguments
    ///
    /// * `check_data`  - Check data to provide signature
    /// * `signature`   - Signature of check data
    /// * `pub_key`     - Public key of the payer
    pub fn pay2pkh(
        check_data: String,
        signature: Signature,
        pub_key: PublicKey,
        address_version: Option<u64>,
    ) -> Self {
        let op_hash_256 = match address_version {
            Some(NETWORK_VERSION_V0) => OpCodes::OP_HASH256_V0,
            Some(NETWORK_VERSION_TEMP) => OpCodes::OP_HASH256_TEMP,
            _ => OpCodes::OP_HASH256,
        };
        let stack = vec![
            StackEntry::Bytes(check_data),
            StackEntry::Signature(signature),
            StackEntry::PubKey(pub_key),
            StackEntry::Op(OpCodes::OP_DUP),
            StackEntry::Op(op_hash_256),
            StackEntry::Bytes(construct_address_for(&pub_key, address_version)),
            StackEntry::Op(OpCodes::OP_EQUALVERIFY),
            StackEntry::Op(OpCodes::OP_CHECKSIG),
        ];
        Self { stack }
    }

    /// Constructs one part of a multiparty transaction script
    ///
    /// ### Arguments
    ///
    /// * `check_data`  - Data to be signed for verification
    /// * `pub_key`     - Public key of this party
    /// * `signature`   - Signature of this party
    pub fn member_multisig(check_data: String, pub_key: PublicKey, signature: Signature) -> Self {
        let stack = vec![
            StackEntry::Bytes(check_data),
            StackEntry::Signature(signature),
            StackEntry::PubKey(pub_key),
            StackEntry::Op(OpCodes::OP_CHECKSIG),
        ];
        Self { stack }
    }

    /// Constructs a multisig locking script
    ///
    /// ### Arguments
    ///
    /// * `m`           - Number of signatures required to unlock
    /// * `n`           - Number of valid signatures total
    /// * `check_data`  - Data to have checked against signatures
    /// * `pub_keys`    - The constituent public keys
    pub fn multisig_lock(m: usize, n: usize, check_data: String, pub_keys: Vec<PublicKey>) -> Self {
        let mut stack = vec![StackEntry::Bytes(check_data), StackEntry::Num(m)];
        stack.append(&mut pub_keys.iter().map(|e| StackEntry::PubKey(*e)).collect());
        stack.push(StackEntry::Num(n));
        stack.push(StackEntry::Op(OpCodes::OP_CHECKMULTISIG));
        Self { stack }
    }

    /// Constructs a multisig unlocking script
    ///
    /// ### Arguments
    ///
    /// * `check_data`  - Data to have signed
    /// * `signatures`  - Signatures to unlock with
    pub fn multisig_unlock(check_data: String, signatures: Vec<Signature>) -> Self {
        let mut stack = vec![StackEntry::Bytes(check_data)];
        stack.append(
            &mut signatures
                .iter()
                .map(|e| StackEntry::Signature(*e))
                .collect(),
        );
        Self { stack }
    }

    /// Constructs a multisig validation script
    ///
    /// ### Arguments
    ///
    /// * `m`           - Number of signatures to assure validity
    /// * `n`           - Number of public keys that are valid
    /// * `signatures`  - Signatures to validate
    /// * `pub_keys`    - Public keys to validate
    pub fn multisig_validation(
        m: usize,
        n: usize,
        check_data: String,
        signatures: Vec<Signature>,
        pub_keys: Vec<PublicKey>,
    ) -> Self {
        let mut stack = vec![StackEntry::Bytes(check_data)];
        stack.append(
            &mut signatures
                .iter()
                .map(|e| StackEntry::Signature(*e))
                .collect(),
        );
        stack.push(StackEntry::Num(m));
        stack.append(&mut pub_keys.iter().map(|e| StackEntry::PubKey(*e)).collect());
        stack.push(StackEntry::Num(n));
        stack.push(StackEntry::Op(OpCodes::OP_CHECKMULTISIG));
        Self { stack }
    }
}

impl From<Vec<StackEntry>> for Script {
    /// Creates a new script with a pre-filled stack
    fn from(s: Vec<StackEntry>) -> Self {
        Script { stack: s }
    }
}
