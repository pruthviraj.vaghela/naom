//! Lexer for ZScript. Breaks a text input down into individual tokens.
//!
//! This module is used mostly for interactive playgrounds and debug purposes.
//! In production, you should always prefer to use binary serialization.

use std::iter;
use std::str::FromStr;

use crate::{OpCodes, StackEntry};
use logos::{Logos, Span};

pub type Lexer<'a> = logos::Lexer<'a, ScriptToken>;

#[derive(Logos, Debug, PartialEq, Eq)]
#[logos(skip r"[ \t\n\f]+")]
pub enum ScriptToken {
    #[regex("OP_[0-9A-Z]+", tokenize_opcode, ignore(case))]
    Operator(OpCodes),
    #[regex("<[0-9a-fA-F][0-9a-fA-F]+>", convert_hex_string)]
    #[regex("\"[^\"]+\"", convert_string_literal)]
    Bitstring(Vec<u8>),
    #[regex("0x[0-9a-fA-F][0-9a-fA-F]?", convert_hex_num)]
    #[regex("[0-9]+", |num| num.slice().parse().ok())]
    Number(usize),
}

impl TryInto<StackEntry> for Result<ScriptToken, ()> {
    type Error = ();

    fn try_into(self) -> Result<StackEntry, ()> {
        match self {
            Ok(ScriptToken::Number(num)) => Ok(StackEntry::Num(num)),
            Ok(ScriptToken::Operator(op)) => Ok(StackEntry::Op(op)),
            Ok(ScriptToken::Bitstring(bytes)) => Ok(StackEntry::Bytes(hex::encode(bytes))),
            Err(()) => Err(()),
        }
    }
}

/// Converts a hex string into a byte sequence.
fn convert_hex_string(lex: &mut Lexer) -> Option<Vec<u8>> {
    let str = lex.slice();
    hex::decode(&str.as_bytes()[1..str.len() - 1]).ok()
}

/// Converts a UTF-8 string literal into a byte sequence.
fn convert_string_literal(lex: &mut Lexer) -> Option<Vec<u8>> {
    let str = lex.slice();
    Some(str.as_bytes()[1..str.len() - 1].to_vec())
}

/// Converts a hex number notation into a number.
fn convert_hex_num(lex: &mut Lexer) -> Option<usize> {
    match usize::from_str_radix(&lex.slice()[2..], 16) {
        Ok(num) => Some(num),
        Err(_) => None,
    }
}

/// Converts an opcode token into an `OpCodes` enum.
fn tokenize_opcode(lex: &mut Lexer) -> Option<OpCodes> {
    match OpCodes::from_str(&lex.slice().to_uppercase()) {
        Ok(opcode) => Some(opcode),
        Err(_) => None,
    }
}

/// Calculates a line/column from a given byte offset.
pub struct LineNums {
    line_offsets: Vec<usize>,
    input_size: usize,
}

impl LineNums {
    pub fn new(input: &str) -> LineNums {
        let line_offsets = input
            .as_bytes()
            .iter()
            .enumerate()
            .filter(|&(_index, char)| *char == b'\n')
            .map(|(index, _)| index + 1);

        LineNums {
            line_offsets: iter::once(0).chain(line_offsets).collect(), // iter::once(0) adds the zeroth line.
            input_size: input.len(),
        }
    }

    /// Returns a line number and a column span for a given byte span. Numbering start from 0.
    /// Returns a `None` if the line number could not be determined (usually because the
    /// span is out of range).
    pub fn location_from_span(&self, span: Span) -> Option<(usize, Span)> {
        if span.start > self.input_size || span.end > self.input_size {
            return None;
        }

        for (line_num, offset) in self.line_offsets.iter().enumerate() {
            if *offset > span.start {
                let prev_line_offset = self.line_offsets.get(line_num - 1).cloned().unwrap_or(0);
                return Some((
                    line_num - 1,
                    Span {
                        start: span.start - prev_line_offset,
                        end: span.end - prev_line_offset,
                    },
                ));
            }
        }

        // default result is the last line
        let num_lines = self.line_offsets.len();
        let last_line_offset = self.line_offsets[num_lines - 1];
        Some((
            num_lines - 1,
            Span {
                start: span.start - last_line_offset,
                end: span.end - last_line_offset,
            },
        ))
    }
}

#[cfg(test)]
mod tests {
    use super::{LineNums, ScriptToken};
    use crate::OpCodes;
    use logos::{Logos, Span};

    #[test]
    fn basic_lexemes() {
        let mut lex = ScriptToken::lexer("2 OP_DUP OP_ADD");

        assert_eq!(lex.next(), Some(Ok(ScriptToken::Number(2))));
        assert_eq!(lex.span(), 0..1);

        assert_eq!(lex.next(), Some(Ok(ScriptToken::Operator(OpCodes::OP_DUP))));
        assert_eq!(lex.span(), 2..8);

        assert_eq!(lex.next(), Some(Ok(ScriptToken::Operator(OpCodes::OP_ADD))));
        assert_eq!(lex.span(), 9..15);

        assert!(lex.next().is_none());
    }

    #[test]
    fn hex_numbers() {
        let mut lex = ScriptToken::lexer("0x2 0xF OP_MUL");

        assert_eq!(lex.next(), Some(Ok(ScriptToken::Number(2))));
        assert_eq!(lex.span(), 0..3);

        assert_eq!(lex.next(), Some(Ok(ScriptToken::Number(15))));
        assert_eq!(lex.span(), 4..7);

        assert_eq!(lex.next(), Some(Ok(ScriptToken::Operator(OpCodes::OP_MUL))));
        assert_eq!(lex.span(), 8..14);

        assert!(lex.next().is_none());
    }

    #[test]
    fn hex_strings() {
        let mut lex = ScriptToken::lexer("0x01 <010203>");

        assert_eq!(lex.next(), Some(Ok(ScriptToken::Number(1))));
        assert_eq!(lex.span(), 0..4);

        assert_eq!(lex.next(), Some(Ok(ScriptToken::Bitstring(vec![1, 2, 3]))));
        assert_eq!(lex.span(), 5..13);

        assert!(lex.next().is_none());
    }

    #[test]
    fn utf_strings() {
        let mut lex = ScriptToken::lexer("0x00 \"hi\" \"こんにちは\"");

        assert_eq!(lex.next(), Some(Ok(ScriptToken::Number(0))));
        assert_eq!(lex.span(), 0..4);

        assert_eq!(lex.next(), Some(Ok(ScriptToken::Bitstring(vec![104, 105]))));
        assert_eq!(lex.span(), 5..9);

        assert_eq!(
            lex.next(),
            Some(Ok(ScriptToken::Bitstring("こんにちは".as_bytes().to_vec())))
        );
        assert_eq!(lex.span(), 10..27);

        assert!(lex.next().is_none());
    }

    #[test]
    fn decimal_numbers() {
        let mut lex = ScriptToken::lexer("3 255");

        assert_eq!(lex.next(), Some(Ok(ScriptToken::Number(3))));

        assert_eq!(lex.next(), Some(Ok(ScriptToken::Number(255))));

        assert!(lex.next().is_none());
    }

    #[test]
    fn convert_line_numbers() {
        let src = "2\n2\nOP_ADD 4\n\nOP_ADD OP_DUP";
        let line_nums = LineNums::new(src);

        let mut lex = ScriptToken::lexer(src);

        let _ = lex.next();
        assert_eq!(
            line_nums.location_from_span(lex.span()).unwrap(),
            (0, Span { start: 0, end: 1 })
        );

        let _ = lex.next();
        assert_eq!(
            line_nums.location_from_span(lex.span()).unwrap(),
            (1, Span { start: 0, end: 1 })
        );

        let _ = lex.next();
        assert_eq!(
            line_nums.location_from_span(lex.span()).unwrap(),
            (2, Span { start: 0, end: 6 })
        );

        let _ = lex.next();
        assert_eq!(
            line_nums.location_from_span(lex.span()).unwrap(),
            (2, Span { start: 7, end: 8 })
        );

        let _ = lex.next();
        assert_eq!(
            line_nums.location_from_span(lex.span()).unwrap(),
            (4, Span { start: 0, end: 6 })
        );

        let _ = lex.next();
        assert_eq!(
            line_nums.location_from_span(lex.span()).unwrap(),
            (4, Span { start: 7, end: 13 })
        );
    }

    #[test]
    fn interpret_lexed_script() {
        use crate::{lang::Script, StackEntry};

        let src = "2 2 OP_ADD 4 OP_EQUAL";
        let mut lex = ScriptToken::lexer(src);
        let mut stack: Vec<StackEntry> = vec![];
        while let Some(stack_entry) = lex.next() {
            stack.push(stack_entry.try_into().unwrap());
        }
        let script = Script::from(stack);
        assert_eq!(script.interpret(), true);

        let src = "3 2 OP_MUL 5 OP_EQUAL";
        let mut lex = ScriptToken::lexer(src);
        let mut stack: Vec<StackEntry> = vec![];
        while let Some(stack_entry) = lex.next() {
            stack.push(stack_entry.try_into().unwrap());
        }
        let script = Script::from(stack);
        assert_eq!(script.interpret(), false);
    }
}
